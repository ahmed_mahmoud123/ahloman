<?php

function fatawa_custom_post() {
	$labels = array(
		'name'               => _x( 'الفتاوى', 'post type general name' ),
		'singular_name'      => _x( 'الفتاوى', 'post type singular name' ),
		'add_new'            => _x( 'أضف جديد', 'book' ),
		'add_new_item'       => __( 'أضف فتوى جديدة' ),
		'edit_item'          => __( 'تعديل فتوى' ),
		'new_item'           => __( 'فتوى جديدة' ),
		'all_items'          => __( 'كل الفتاوى' ),
		'view_item'          => __( 'عرض الفتوى' ),
		'search_items'       => __( 'البحث في الفتاوى' ),
		'not_found'          => __( 'لا يوجد فتاوى' ),
		'not_found_in_trash' => __( 'لا يوجد فتاوى في سلة المهملات' ), 
		'parent_item_colon'  => '',
		'menu_name'          => 'الفتاوى'
	);
	$args = array(
		'labels'        => $labels,
		'description'   => '',
		'public'        => true,
		'menu_position' => 5,
		'supports'      => array( 'thumbnail', 'comments','author','title' ),
		'has_archive'   => true,
		'exclude_from_search'   => false,
	);
	register_post_type( 'fatawa', $args );	
}
add_action( 'init', 'fatawa_custom_post' );

function fatawa_updated_messages( $messages ) {
	global $post, $post_ID;
	$messages['fatawa'] = array(
		0 => '', 
		1 => sprintf( __('تم التحديث. <a href="%s">عرض الفتوى</a>'), esc_url( get_permalink($post_ID) ) ),
		2 => __('تم تحديث العنصر.'),
		3 => __('تم حذف العنصر.'),
		4 => __('تم تحديث الفتوى.'),
		5 => isset($_GET['revision']) ? sprintf( __('تم استعادة الفتوى، من %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('تم النشر. <a href="%s">عرض الفتوى</a>'), esc_url( get_permalink($post_ID) ) ),
		7 => __('تم حفظ الفتوى.'),
		8 => sprintf( __('تم ارسال الفتوى. <a target="_blank" href="%s">عرض الفتوى</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
		9 => sprintf( __('تم تأريخ الفتوى للنشر: <strong>%1$s</strong>. <a target="_blank" href="%2$s">عرض الفتوى</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
		10 => sprintf( __('تم تحديث الفتوى كمسوده. <a target="_blank" href="%s">عرض الفتوى</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
	);
	return $messages;
}
add_filter( 'fatawa_post_updated_messages', 'fatawa_updated_messages' );

function fatawa_taxonomies() {
	$labels = array(
		'name'              => _x( 'تصنيفات الفتاوى', 'taxonomy general name' ),
		'singular_name'     => _x( 'تصنيف الفتاوى', 'taxonomy singular name' ),
		'search_items'      => __( 'البحث في تصنيفات الفتاوى' ),
		'all_items'         => __( 'جميع تصنيفات الفتاوى' ),
		'parent_item'       => __( 'التصنيف الأب لتصنيف الفتاوى' ),
		'parent_item_colon' => __( 'التصنيف الأب لتصنيف الفتاوى:' ),
		'edit_item'         => __( 'تعديل تصنيف الفتاوى' ), 
		'update_item'       => __( 'تحديث تصنيف الفتاوى' ),
		'add_new_item'      => __( 'إضافة تصنيف للفتاوى' ),
		'new_item_name'     => __( 'تصنيف جديد للفتاوى' ),
		'menu_name'         => __( 'تصنيفات الفتاوى' ),
	);
	$args = array(
		'labels'				=> $labels,
		'hierarchical'			=> true,
		'public'				=> true,
		'show_ui'				=> true,
		'show_admin_column'		=> true,
		'show_in_nav_menus'		=> true,
		'show_tagcloud'			=> true,
	);
	register_taxonomy( 'fatawa_category', 'fatawa', $args );
}
add_action( 'init', 'fatawa_taxonomies', 0 );

function add_fatawa_meta_box() {  
    add_meta_box(  
        'fatawa_meta_box', // $id  
        'محتوى الفتوى', // $title   
        'show_fatawa_meta_box', // $callback  
        'fatawa', // $page  
        'normal', // $context  
        'high'); // $priority  
}

  
add_action('add_meta_boxes', 'add_fatawa_meta_box'); 

$prefix = 'fatawa_';  
$fatawa_meta_fields = array(  
	    array(  
	        'label'=> 'السؤال',  
	        'desc'  => 'سؤال الفتوى',  
	        'id'    => $prefix.'question',  
	        'type'  => 'textarea'  
	    ),  
		
		
		array(  
	        'label'=> 'الجواب',  
	        'desc'  => 'جواب الفتوى',  
	        'id'    => $prefix.'answer',  
	        'type'  => 'textarea'  
	    ),  
	    
		array(  
	        'label'=> 'الفتوى الصوتيه',  
	        'desc'  => 'رابط الفتوى الصوتيه',  
	        'id'    => $prefix.'audio',  
	        'type'  => 'text'  
	    ), 
	);

	// The Callback
function show_fatawa_meta_box() {
global $fatawa_meta_fields, $post;
// Use nonce for verification
echo '<input type="hidden" name="fatawa_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';
	
	// Begin the field table and loop
	echo '<table class="form-table">';
	foreach ($fatawa_meta_fields as $field) {
		// get value of this field if it exists for this post
		$meta = get_post_meta($post->ID, $field['id'], true);
		// begin a table row with
		echo '<tr>
				<th><label for="'.$field['id'].'">'.$field['label'].'</label></th>
				<td>';
				switch($field['type']) {
				// case items will go here
					// text
					case 'text':
						echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" />
							<br /><span class="description">'.$field['desc'].'</span>';
					break;	
					
					// textarea  
					case 'textarea':  
					   wp_editor( $meta, $field['id'], array( 'textarea_name' => $field['id'], 'teeny' => FALSE ) ); 
					break; 
				} //end switch
		echo '</td></tr>';
	} // end foreach
	echo '</table>'; // end table
}

// Save the Data
function save_fatawa_meta($post_id) {
    global $fatawa_meta_fields;
	
	// verify nonce
	if (!wp_verify_nonce($_POST['fatawa_meta_box_nonce'], basename(__FILE__))) 
		return $post_id;
	// check autosave
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
		return $post_id;
	// check permissions
	if ('page' == $_POST['post_type']) {
		if (!current_user_can('edit_page', $post_id))
			return $post_id;
		} elseif (!current_user_can('edit_post', $post_id)) {
			return $post_id;
	}
	
	// loop through fields and save the data
	foreach ($fatawa_meta_fields as $field) {
		$old = get_post_meta($post_id, $field['id'], true);
		$new = $_POST[$field['id']];
		if ($new && $new != $old) {
			update_post_meta($post_id, $field['id'], $new);
		} elseif ('' == $new && $old) {
			delete_post_meta($post_id, $field['id'], $old);
		}
	} // end foreach
}
add_action('save_post', 'save_fatawa_meta');  
  


?>