<?php

function books_custom_post() {
	$labels = array(
		'name'               => _x( 'الكتب', 'post type general name' ),
		'singular_name'      => _x( 'الكتب', 'post type singular name' ),
		'add_new'            => _x( 'أضف جديد', 'book' ),
		'add_new_item'       => __( 'أضف كتاب جديد' ),
		'edit_item'          => __( 'تعديل كتاب' ),
		'new_item'           => __( 'كتاب جديد' ),
		'all_items'          => __( 'كل الكتب' ),
		'view_item'          => __( 'عرض الكتاب' ),
		'search_items'       => __( 'البحث في الكتب' ),
		'not_found'          => __( 'لا يوجد كتب' ),
		'not_found_in_trash' => __( 'لا يوجد كتب في سلة المهملات' ), 
		'parent_item_colon'  => '',
		'menu_name'          => 'الكتب'
	);
	$args = array(
		'labels'        => $labels,
		'description'   => '',
		'public'        => true,
		'menu_position' => 5,
		'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments','author' ),
		'has_archive'   => true,
		'exclude_from_search'   => false,
	);
	register_post_type( 'books', $args );	
}
add_action( 'init', 'books_custom_post' );

function books_updated_messages( $messages ) {
	global $post, $post_ID;
	$messages['books'] = array(
		0 => '', 
		1 => sprintf( __('تم التحديث. <a href="%s">عرض الكتاب</a>'), esc_url( get_permalink($post_ID) ) ),
		2 => __('تم تحديث العنصر.'),
		3 => __('تم حذف العنصر.'),
		4 => __('تم تحديث الكتاب.'),
		5 => isset($_GET['revision']) ? sprintf( __('تم استعادة الكتاب، من %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('تم النشر. <a href="%s">عرض الكتاب</a>'), esc_url( get_permalink($post_ID) ) ),
		7 => __('تم حفظ الكتاب.'),
		8 => sprintf( __('تم ارسال الكتاب. <a target="_blank" href="%s">عرض الكتاب</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
		9 => sprintf( __('تم تأريخ الكتاب للنشر: <strong>%1$s</strong>. <a target="_blank" href="%2$s">عرض الكتاب</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
		10 => sprintf( __('تم تحديث الكتاب كمسوده. <a target="_blank" href="%s">عرض الكتاب</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
	);
	return $messages;
}
add_filter( 'books_post_updated_messages', 'books_updated_messages' );

function books_taxonomies() {
	$labels = array(
		'name'              => _x( 'تصنيفات الكتب', 'taxonomy general name' ),
		'singular_name'     => _x( 'تصنيف الكتب', 'taxonomy singular name' ),
		'search_items'      => __( 'البحث في تصنيفات الكتب' ),
		'all_items'         => __( 'جميع تصنيفات الكتب' ),
		'parent_item'       => __( 'التصنيف الأب لتصنيف الكتب' ),
		'parent_item_colon' => __( 'التصنيف الأب لتصنيف الكتب:' ),
		'edit_item'         => __( 'تعديل تصنيف الكتب' ), 
		'update_item'       => __( 'تحديث تصنيف الكتب' ),
		'add_new_item'      => __( 'إضافة تصنيف للالكتب' ),
		'new_item_name'     => __( 'تصنيف جديد للالكتب' ),
		'menu_name'         => __( 'تصنيفات الكتب' ),
	);
	$args = array(
		'labels'				=> $labels,
		'hierarchical'			=> true,
		'public'				=> true,
		'show_ui'				=> true,
		'show_admin_column'		=> true,
		'show_in_nav_menus'		=> true,
		'show_tagcloud'			=> true,
	);
	register_taxonomy( 'book_category', 'books', $args );
}
add_action( 'init', 'books_taxonomies', 0 );
function add_book_meta_box() {  
    add_meta_box(  
        'book_meta_box', // $id  
        'بيانات عن الكتاب', // $title   
        'show_book_meta_box', // $callback  
        'books', // $page  
        'normal', // $context  
        'high'); // $priority  
}  
add_action('add_meta_boxes', 'add_book_meta_box'); 

$prefix = 'book_';  
$book_meta_fields = array(  

		array(  
	        'label'=> 'مؤلف الكتاب',  
	        'desc'  => 'إسم مؤلف الكتاب',  
	        'id'    => $prefix.'author',  
	        'type'  => 'text'  
	    ), 
	    		array(  
	        'label'=> 'رابط وورد أو PDF',  
	        'desc'  => 'رابط PDF',  
	        'id'    => $prefix.'file',  
	        'type'  => 'text'  
	    )
	);

	// The Callback
function show_book_meta_box() {
global $book_meta_fields, $post;
// Use nonce for verification
echo '<input type="hidden" name="book_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';
	
	// Begin the field table and loop
	echo '<table class="form-table">';
	foreach ($book_meta_fields as $field) {
		// get value of this field if it exists for this post
		$meta = get_post_meta($post->ID, $field['id'], true);
		// begin a table row with
		echo '<tr>
				<th><label for="'.$field['id'].'">'.$field['label'].'</label></th>
				<td>';
				switch($field['type']) {
				// case items will go here
					// text
					case 'text':
						echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" />
							<br /><span class="description">'.$field['desc'].'</span>';
					break;	
					
					// textarea  
					case 'textarea':  
					   wp_editor( $meta, $field['id'], array( 'textarea_name' => $field['id'], 'teeny' => FALSE ) ); 
					break; 
				} //end switch
		echo '</td></tr>';
	} // end foreach
	echo '</table>'; // end table
}

// Save the Data
function save_book_meta($post_id) {
    global $book_meta_fields;
	
	// verify nonce
	if (!wp_verify_nonce($_POST['book_meta_box_nonce'], basename(__FILE__))) 
		return $post_id;
	// check autosave
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
		return $post_id;
	// check permissions
	if ('page' == $_POST['post_type']) {
		if (!current_user_can('edit_page', $post_id))
			return $post_id;
		} elseif (!current_user_can('edit_post', $post_id)) {
			return $post_id;
	}
	
	// loop through fields and save the data
	foreach ($book_meta_fields as $field) {
		$old = get_post_meta($post_id, $field['id'], true);
		$new = $_POST[$field['id']];
		if ($new && $new != $old) {
			update_post_meta($post_id, $field['id'], $new);
		} elseif ('' == $new && $old) {
			delete_post_meta($post_id, $field['id'], $old);
		}
	} // end foreach
}
add_action('save_post', 'save_book_meta');  
  


?>