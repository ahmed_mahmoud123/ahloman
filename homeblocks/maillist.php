			<div class="box">
				<div class="boxtop">
					<div class="titlebg">
						<div class="title">
							<div class="icon icon03"></div> القائمة البريدية
						</div>
					</div>
				</div>
				<div class="boxcenter">
					<div class="blocksmall">
						<div class="maillist">
							اشترك معنا في القائمة البريدية ليصلك جديدنا
							<?php 
                                $widgetNL = new WYSIJA_NL_Widget(true);
                                echo $widgetNL->widget(array('form' => 2, 'form_type' => 'php')); 
                            ?>
						</div>
					</div>
				</div>
			</div>