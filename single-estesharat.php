<?php get_header(); ?>
<?php
	include(TEMPLATEPATH."/sidebar2.php");
	$terms_as_text = get_the_term_list( $post->ID, 'estesharat_category') ;
?>
		<div class="topcontentright">
			<div class="box">
				<div class="boxtop">
					<div class="titlebg">
						<div class="title">
							<div class="icon icon01"></div> <?php echo strip_tags($terms_as_text, ''); ?>
						</div>
					</div>
				</div>
				<div class="boxcenter">
					<div class="insidesinglepost">
						
					<?php if (have_posts()) : ?>
						<?php while (have_posts()) : the_post(); 
							$esteshara_question = get_post_meta($post->ID, 'esteshara_question', true); 
							$esteshara_answer = get_post_meta($post->ID, 'esteshara_answer', true);
							$esteshara_audio = get_post_meta($post->ID, 'esteshara_audio', true);	
						?>
							
						<h1 class="title"><?php the_title() ?></h1>
						<p>رقم الاستشارة: <?php echo  taxonomy_seq($post->ID,'estesharat') ?></p>
					<?php if($esteshara_question): ?>
					<p><span style="color:#ff0000">السؤال:</span></p>
						<p><?php echo $esteshara_question ?></p>
					<?php endif; ?>
					<?php if($esteshara_answer): ?>
					<p><span style="color:#ff0000">الجواب:</span></p>
						<p><?php echo $esteshara_answer; ?></p>
					<?php endif; ?>
					<?php if($esteshara_audio): ?>
						<p>
							<audio controls>
								  <source src="<?php echo $esteshara_audio ?>" type="audio/mpeg">
								Your browser does not support the audio element.
							</audio>
						</p>
					<?php endif; ?>
					<?php if(function_exists('pf_show_link')){echo pf_show_link();} ?>
						<?php endwhile; ?>
						<div class="share-btns">
							<span class='st_sharethis_hcount' displayText='ShareThis'></span>
							<span class='st_facebook_hcount' displayText='Facebook'></span>
							<span class='st_twitter_hcount' displayText='Tweet'></span>
							<span class='st_linkedin_hcount' displayText='LinkedIn'></span>
							<span class='st_pinterest_hcount' displayText='Pinterest'></span>
							<span class='st_email_hcount' displayText='Email'></span>
						</div>
					<?php endif; ?>
					<div class="spacerline"></div>
					</div>
				</div>
				<?php comments_template(); ?>
			</div>
		</div>

<?php get_footer(); ?>