<?php
remove_action('pre_get_posts', 'custom_author_archive');
get_header();
?>
		
	  <?php      
			//getting current author name
            if(isset($_GET['author_name'])) :

                $curauth = get_userdatabylogin($author_name);

            else :

                $curauth = get_userdata(intval($author));

            endif;      
			
			if($_GET['post'])
			$post_type = $_GET['post'];
			else {
			$post_type = array('post','audio','video','fatawa','estesharat','books');
			//$post_type = array('post');
			}		
			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
			$author = new WP_Query( array(
		        'author' => $_GET['author'],
			    'post_type' => $post_type ,
			    'posts_per_page' => 10,
			    'paged' => $paged
				 )
			);
     ?>
     
     <?php $max_num_pages=$author->max_num_pages ?>


<?php include(TEMPLATEPATH."/sidebar.php"); ?>

		<div class="topcontentright">
			<div class="box">
				<div class="boxtop">
					<div class="titlebg">
						<div class="title">
							<div class="icon icon11"></div> مقالات بواسطة <?php echo $curauth->nickname ?>
						</div>
					</div>
				</div>
				<div class="boxcenter">
					<div class="archivesinglelist">
						<div class="file-info author-box">
							<?php echo get_avatar( get_the_author_meta('user_email', $curauth->id ), 100 ); ?>
							<div class="author-informations">
								<p> 
									<span>- <?php echo $curauth->display_name ?> 
								</p></span>
			<!--				<p>
									<span>المنطقة</span>
								</p>
			    !-->				<p>
									<span>. تاريخ الاضافة:</span>
									 <?php the_time('d-m-Y'); ?>
								</p>
								<?php if(function_exists('the_views')) { ?><p><span>. عدد الزيارات:</span>  <?php
									global $wpdb, $simplehitcounter_table_name;
                                     $simplehitcounter_table_name = $wpdb->prefix . "author_hit";
									if ($wpdb->get_var("SHOW TABLES LIKE '$simplehitcounter_table_name'") != $simplehitcounter_table_name) {
		                              $wpdb->query("CREATE TABLE $simplehitcounter_table_name (
			                          author_id INT NOT NULL UNIQUE,
	                            	  hit_count INT NOT NULL
		                              )");
									}
			                          $wpdb->query("INSERT INTO $simplehitcounter_table_name (author_id, hit_count) VALUES ($curauth->id, 0)");
		                  $wpdb->query("UPDATE $simplehitcounter_table_name SET hit_count = hit_count + 1 WHERE  author_id=$curauth->id");
                       	echo $wpdb->get_var("SELECT hit_count FROM $simplehitcounter_table_name WHERE  author_id=$curauth->id");
									
									?></p><?php } ?>
							</div>
						</div>
						<table class="total">
							<thead>
								<tr>
									<th><a href="http://ahloman.net/?author=<?php echo $curauth->id ?>&post=post">المقالات</a></th>
									<th><a href="http://ahloman.net/?author=<?php echo $curauth->id ?>&post=books">الكتب</a></th>
									<th><a href="http://ahloman.net/?author=<?php echo $curauth->id ?>&post=video">المرئيات</a></th>
									<th><a href="http://ahloman.net/?author=<?php echo $curauth->id ?>&post=audio">الصوتيات</a></th>
									<th><a href="http://ahloman.net/?author=<?php echo $curauth->id ?>&post=fatawa">الفتاوى و الأحكام</a></th>
									<th><a href="http://ahloman.net/?author=<?php echo $curauth->id ?>&post=estesharat">الإستشارات والحلول</a></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<a href="http://ahloman.net/?author=<?php echo $curauth->id ?>&post=post">
										<?php
										$post_author = $curauth->id; //author id
										$count = $wpdb->get_var( "SELECT COUNT(*) FROM $wpdb->posts WHERE post_author = $post_author AND post_type IN ('post') and post_status = 'publish'" );
										echo $count ?>
										</a>
									</td>
									<td>
										<a href="http://ahloman.net/?author=<?php echo $curauth->id ?>&post=books">
										<?php
										$post_author = $curauth->id; //author id
										$count = $wpdb->get_var( "SELECT COUNT(*) FROM $wpdb->posts WHERE post_author = $post_author AND post_type IN ('books') and post_status = 'publish'" );
										echo $count ?>
										</a>
									</td>
									<td>
										<a href="http://ahloman.net/?author=<?php echo $curauth->id ?>&post=video">
										<?php
										$post_author = $curauth->id; //author id
										$count = $wpdb->get_var( "SELECT COUNT(*) FROM $wpdb->posts WHERE post_author = $post_author AND post_type IN ('video') and post_status = 'publish'" );
										echo $count ?>
										</a>
									</td>
									<td>
										<a href="http://ahloman.net/?author=<?php echo $curauth->id ?>&post=audio">
										<?php
										$post_author = $curauth->id; //author id
										$count = $wpdb->get_var( "SELECT COUNT(*) FROM $wpdb->posts WHERE post_author = $post_author AND post_type IN ('audio') and post_status = 'publish'" );
										echo $count ?>
										</a>
									</td>
									<td>
										<a href="http://ahloman.net/?author=<?php echo $curauth->id ?>&post=fatawa">
										<?php
										$post_author = $curauth->id; //author id
										$count = $wpdb->get_var( "SELECT COUNT(*) FROM $wpdb->posts WHERE post_author = $post_author AND post_type IN ('fatawa') and post_status = 'publish'" );
										echo $count ?>
										</a>
									</td>
									<td>
										<a href="http://ahloman.net/?author=<?php echo $curauth->id ?>&post=estesharat">
										<?php
										$post_author = $curauth->id; //author id
										$count = $wpdb->get_var( "SELECT COUNT(*) FROM $wpdb->posts WHERE post_author = $post_author AND post_type IN ('estesharat') and post_status = 'publish'" );
										echo $count ?>
										</a>
									</td>
								</tr>
								<tr class="total-count">
									<td>الاجمالى</td>
									<td colspan="5">
									<?php
									$post_author = $curauth->id; //author id
									$count = $wpdb->get_var( "SELECT COUNT(*) FROM $wpdb->posts WHERE post_author = $post_author AND post_type IN ('post','audio','video','fatawa','estesharat','books') and post_status = 'publish'" );
									echo $count ?>
									</td>
								</tr>
							</tbody>
						</table>
						<div class="social-links">
							<?php if ( get_the_author_meta( 'facebook', $curauth->id ) != '' )  { ?>
							<a target="_blank" href="<?php echo esc_url( get_the_author_meta( 'facebook', $curauth->id ) ); ?>">
								<img src="<?php bloginfo('template_directory'); ?>/images/facebook500.png" alt="" />
							</a>
							<?php } ?> 
							<?php if ( get_the_author_meta( 'twitter', $curauth->id ) != '' )  { ?>
							<a target="_blank" href="https://twitter.com/<?php echo wp_kses( get_the_author_meta( 'twitter', $curauth->id ), null ); ?>">
								<img src="<?php bloginfo('template_directory'); ?>/images/twitter500.png" alt="" />
							</a>
							<?php } ?>
							<?php if ( get_the_author_meta( 'googleplus', $curauth->id ) != '' )  { ?>
							<a target="_blank" href="<?php echo esc_url( get_the_author_meta( 'googleplus', $curauth->id ) ); ?>">
								<img src="<?php bloginfo('template_directory'); ?>/images/googleplus500.png" alt="" />
							</a>
							<?php } ?>
							<?php if ( get_the_author_meta( 'youtube', $curauth->id ) != '' )  { ?>
							<a target="_blank" href="<?php echo esc_url( get_the_author_meta( 'youtube', $curauth->id ) ); ?>">
								<img src="<?php bloginfo('template_directory'); ?>/images/youtube500.png" alt="" />
							</a>
							<?php } ?>
						</div>
						<ul class="author-links">
							<li><a href="http://ahloman.net/?author=<?php echo $curauth->id ?>&post=video">مرئيات</a></li>
							<li><a href="http://ahloman.net/?author=<?php echo $curauth->id ?>&post=audio">صوتيات</a></li>
							<li><a href="http://ahloman.net/?author=<?php echo $curauth->id ?>&post=fatawa">فتاوى و أحكام</a></li>
							<li><a href="http://ahloman.net/?author=<?php echo $curauth->id ?>&post=estesharat">إستشارات وحلول</a></li>
							<li><a href="http://ahloman.net/?author=<?php echo $curauth->id ?>&post=post">مقالات</a></li>
						</ul>
						<ul>
							<li>
								<ul>
									<?php if($author->have_posts()): while($author->have_posts()): $author->the_post() ?>
									<li>
										<div class="title">
											<h1><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h1>
											<?php the_excerpt() ?>
										</div>
										<div class="image"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php if ( has_post_thumbnail() ) { the_post_thumbnail( 'thumb75' ); } ?></a></div>
										<div class="date"><span><strong>تاريخ الإضافة:</strong> <?php the_time('d-m-Y'); ?></span><?php if(function_exists('the_views')) { ?><span><strong>زيارة:</strong> <?php the_views(); ?></span><?php } ?><span><strong>التعليقات:</strong> <?php comments_number( 'لا يوجد تعليقات', 'تعليق واحد', '%' ); ?></span></div>
									</li>
								<?php endwhile; ?>
									<li>
										<div class="navigation">
											<div class="alignright">
												<?php next_posts_link(' السابق ',$author->max_num_pages); ?>
											</div>
											<div class="alignleft">
												<?php previous_posts_link(' التالي '); ?>
											</div>
										</div>
									</li>
									<?php wp_reset_postdata(); ?>
								<?php else: ?>
										<li>
											<div class="title">
												<h1>عفواً ..</h1>
												
<p>لا يوجد مواد في هذا القسم في الوقت الحالي .</p>
											</div>
											<div class="image"></div>
										</li>
									<?php  endif; ?>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
<?php get_footer(); ?>