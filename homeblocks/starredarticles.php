<?php
	// Head Master Articles
	$option_starredarticles = get_option('ahloman_starredarticles');
	$option_starredarticles_check = get_option('ahloman_starredarticles_check');
?>
	<?php if($option_starredarticles_check == true) { ?>
	<div class="starredarticles">
		<h1><a href="<?php $starredarticles = get_category_link( $option_starredarticles ); echo $starredarticles; ?>" alt="<?php get_cat_name( $option_starredarticles ); ?>" title="<?php get_cat_name( $option_starredarticles ); ?>"><?php echo get_cat_name($option_starredarticles); ?></a></h1>
		<span id="mqslidernext" class="leftbutton"></span>
		<span id="mqsliderprev" class="rightbutton"></span>
		<div class="content">
			<ul id="starredarticles">
				<li>
					<ul>
						<?php $linecount = 0; ?>
						<?php $recent = new WP_Query("cat=".$option_starredarticles."&showposts=40&orderby=last"); while($recent->have_posts()) : $recent->the_post();?>
						<li>
							<div class="title">
								<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
								<div class="author"><?php the_author_posts_link(); ?></div>
							</div>
							<div class="image"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php echo get_avatar( get_the_author_meta( 'ID' ), 80 ); ?></a></div>
						</li>
						<?php if ($linecount == 7) : ?>
								</ul>
							</li>
							<li>
								<ul>
							<?php endif; ?>
						<?php if ($linecount == 15) : ?>
								</ul>
							</li>
							<li>
								<ul>
							<?php endif; ?>
						<?php if ($linecount == 23) : ?>
								</ul>
							</li>
							<li>
								<ul>
							<?php endif; ?>
						<?php if ($linecount == 31) : ?>
								</ul>
							</li>
							<li>
								<ul>
							<?php endif; ?>
						<?php if ($linecount == 39) : ?>
								</ul>
							</li>
							<li>
								<ul>
							<?php endif; ?>
						<?php $linecount++; ?>
						<?php endwhile; ?>
					</ul>
				</li>
			</ul>
		</div>
		<div class="buttons">
			<ul>
				<li class="more"><a href="<?php $starredarticles = get_category_link( $option_starredarticles ); echo $starredarticles; ?>" alt="<?php get_cat_name( $option_starredarticles ); ?>" title="<?php get_cat_name( $option_starredarticles ); ?>">كل المقالات</a></li>
			</ul>
		</div>
	</div>
	<?php } ?>