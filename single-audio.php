<?php get_header(); ?>
<!-- AddThis Smart Layers BEGIN -->
<!-- Go to http://www.addthis.com/get/smart-layers to customize >
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-528ccbfe64614c0b"></script>
<script type="text/javascript">
	var rawString = document.title;
    rawString = rawString.replace(/ُ|ِ|َ|ٍ|ً|ّ/g,"");
	var addthis_config = addthis_config||{};
	addthis_config.ui_email_subject = rawString;
  	addthis.layers({
	    'theme' : 'transparent',
	    'share' : {
	      'position' : 'right',
	      'numPreferredServices' : 5
	    },   
  	});
  	console.log(rawString);
</script>
< AddThis Smart Layers END -->
<?php
	include(TEMPLATEPATH."/sidebar2.php");
	$terms_as_text = get_the_term_list( $post->ID, 'audio_category') ;
    
	//getting current author name
    if(isset($_GET['author_name'])) :

        $curauth = get_userdatabylogin($author_name);

    else :

        $curauth = get_userdata(intval($author));

    endif;
?>
		<div class="topcontentright">
			<div class="box">
				<div class="boxtop">
					<div class="titlebg">
						<div class="title">
							<div class="icon icon01"></div> <?php echo strip_tags($terms_as_text, ''); ?>
						</div>
					</div>
				</div>
				<div class="boxcenter">
					<div class="insidesinglepost">
					<h1 class="title"><?php wp_title(''); ?></h1><br />
					
							
					<?php if (have_posts()) : ?>
						<?php while (have_posts()) : the_post(); 
						$audio_text = get_post_meta($post->ID, 'audio_text', true); 
						?>
						<br />
						<audio class="auto-player-single" controls>
							  <source src="<?php echo $audio_text ?>" type="audio/mpeg">
							Your browser does not support the audio element.
						</audio>
						<h3><span class="download" ></span> <?php echo do_shortcode( '[download label="تحميل" ]'.$audio_text.'[/download]' ); ?> </h3>
						<p><?php the_content(); ?></p>
						<div class="file-info">
							<p>
								<span>- <?php the_author(); ?></span>
								
							</p>
							<p>
								<span>. تاريخ الاضافة:</span>
								 <?php the_time('d-m-Y'); ?>
							</p>
							<p>
								<span>. التصنيف:</span>
								<?php the_terms( $post->ID, 'audio_category', '', ' / ' ); ?>
							</p>
							<?php if(function_exists('the_views')) { ?><p><span>. عدد مرات الاستماع:</span> <?php the_views(); ?></p><?php } ?>
						</div>
						<?php endwhile; ?>
						<div class="share-btns">
							<span class='st_sharethis_hcount' displayText='ShareThis'></span>
							<span class='st_facebook_hcount' displayText='Facebook'></span>
							<span class='st_twitter_hcount' displayText='Tweet'></span>
							<span class='st_linkedin_hcount' displayText='LinkedIn'></span>
							<span class='st_pinterest_hcount' displayText='Pinterest'></span>
							<span class='st_email_hcount' displayText='Email'></span>
						</div>
					<?php endif; ?>
					<div class="spacerline"></div>
					</div>
				</div>
				<?php comments_template(); ?>
			</div>
		</div>
	
<?php get_footer(); ?>