<?php
	// Media
	$option_vedio = get_option('ahloman_vedio');
	$option_vedio_check = get_option('ahloman_vedio_check');
	$option_media = get_option('ahloman_media');
	$option_media_check = get_option('ahloman_media_check');
	$option_mediablock_check = get_option('ahloman_mediablock_check');
?>

<?php if($option_mediablock_check == true) { ?>
		<div class="box">
			<div class="boxtop">
				<div class="titlebg">
					<div class="title">
						<div class="icon icon04"></div> مرئيات وصوتيات
					</div>
				</div>
			</div>
			<div class="boxcenter">
				<div class="media">
					<?php if($option_media_check == true) { ?>
					<div class="voice">
						<div class="title icon05">المكتبة الصوتية</div>
						<div class="content">
							<script>
								$(function(){
									$(".player-control button").click(function(){
										$(this).parent().toggleClass("playing");
									});
								});
							</script>
							<ul>
								<?php $recent = new WP_Query(array('post_type'=>'audio' , 'posts_per_page'=>4)); while($recent->have_posts()) : $recent->the_post();?>
								<?php $val = get_post_meta($post->ID, 'audio_text', true);?>
								<li>
									<div class="name"><a href="<?php the_permalink(); ?>"> <?php the_title() ?></a></div>
									<div class="icons">
										<audio preload="none" id="<?php the_ID() ?>" style="width: 35px;">
											<source src="<?php echo $val ?>" type="audio/mpeg">
											Your browser does not support the audio element.
										</audio>
										<div class="player-control">
									
											<button class="pc-play" onclick="document.getElementById('<?php the_ID() ?>').play()"></button>
											<button class="pc-stop" onclick="document.getElementById('<?php the_ID() ?>').pause()"></button> 
									
									
										
										</div>
                                        <?php echo do_shortcode( '[download label="حمل"]'.$val.'[/download]' ); ?>
									
									</div>
									
								</li>	
							<?php endwhile; ?>
							</ul>
						</div> 
					</div>
					<?php } ?>
					
					
				<div class="vedio">
						<div id="vedios">
						<?php $video = new WP_Query(array(
									'post_type' => 'video',
									'posts_per_page' => '4',
									'tax_query' => array(
														array(
															'taxonomy' => 'video_category',
															'field' => 'term_id',
															'terms' => '61'
														)
								))); 
								while($video->have_posts()) : $video->the_post();
								$video_text = get_post_meta($post->ID, 'video_text', true);
								$video_youtube = get_post_meta($post->ID, 'video_youtube', true);
								?>
						<div id ="vedio<?php the_ID(); ?>" class="bigthumb">
							<?php if(!$video_youtube && $video_text): ?>
								<video preload="none" width="350" height="225" controls>
								  <source src="<?php echo $video_text; ?>" type="video/mp4">
								  
									Your browser does not support the video tag.
								</video>
							<?php elseif($video_youtube && !$video_text): ?>
								<iframe width="350" height="225" src="http://www.youtube.com/embed/<?php echo $video_youtube; ?>" frameborder="0" allowfullscreen></iframe>
							<?php elseif($video_text && $video_youtube): ?>
								<iframe title="YouTube video player" class="youtube-player" type="text/html" width="350" height="225" src="http://www.youtube.com/embed/<?php echo $video_youtube; ?>" frameborder="0" allowFullScreen></iframe>
							<?php endif; ?>
						</div>
						<?php endwhile; ?>
						<div class="items">
							<ul>
								<?php $video = new WP_Query(array(
									'post_type' => 'video',
									'posts_per_page' => '4',
									'tax_query' => array(
														array(
															'taxonomy' => 'video_category',
															'field' => 'term_id',
															'terms' => '61'
														)
										)
								)); 
								while($video->have_posts()) : $video->the_post();?>
								<li>
									<div class="title"><a href="#vedio<?php the_ID(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
										<span><?php echo get_the_author_meta( 'display_name', $curauth->id ); ?></span>
									</div>
									<div class="image"><?php if ( has_post_thumbnail() ) { the_post_thumbnail( 'thumb40' ); } ?></div>
								</li>
								<?php endwhile; ?>
							</ul>
						</div>
					</div>
				</div>
				</div>
			</div>
		</div>
<?php } ?>