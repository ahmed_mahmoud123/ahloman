<?php
	//Slider
	$option_slider = get_option('ahloman_slider');
	$option_slider_check = get_option('ahloman_slider_check');
?>

		<div class="rightcontent">
		
			<div class="slider">
				<div id="topslider" class="sliderbox">
					<div class="titles" id="slider-nav">
							<?php $video = new WP_Query(array(
									'post_type' => 'video',
									'posts_per_page' => '4'
								)); 
								$i = 0;
								while($video->have_posts()) : $video->the_post();?>
								<a data-slide-index="<?php echo $i++  ?>" href=""><?php the_title(); ?></a>
							<?php endwhile; ?>
					</div>
					<div id="top-slider-holder">
						<?php $video = new WP_Query(array(
										'post_type' => 'video',
										'posts_per_page' => '4'
									)); 
									while($video->have_posts()) : $video->the_post();
									$video_text = get_post_meta($post->ID, 'video_text', true);
									$video_youtube = get_post_meta($post->ID, 'video_youtube', true);
									?>
						<div id="slideritem-<?php the_ID(); ?>" class="images"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php if ( has_post_thumbnail() ) { the_post_thumbnail( 'slider' ); } ?></a></div>
						<?php endwhile; ?>
					</div>
				</div>
				<div class="slideritems"></div>
			</div>
		</div>

