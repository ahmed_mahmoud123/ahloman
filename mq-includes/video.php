<?php

function video_custom_post() {
	$labels = array(
		'name'               => _x( 'المرئيات', 'post type general name' ),
		'singular_name'      => _x( 'المرئيات', 'post type singular name' ),
		'add_new'            => _x( 'أضف جديد', 'video' ),
		'add_new_item'       => __( 'أضف ملف مرئى جديدة' ),
		'edit_item'          => __( 'تعديل الملف' ),
		'new_item'           => __( 'مقاله مرئيه جديدة' ),
		'all_items'          => __( 'كل المرئيات' ),
		'view_item'          => __( 'عرض المقاله المرئيه' ),
		'search_items'       => __( 'البحث في المرئيات' ),
		'not_found'          => __( 'لا يوجد مرئيات' ),
		'not_found_in_trash' => __( 'لا يوجد مرئيات في سلة المهملات' ), 
		'parent_item_colon'  => '',
		'menu_name'          => 'المرئيات'
	);
	$args = array(
		'labels'        => $labels,
		'description'   => '',
		'public'        => true,
		'menu_position' => 5,
		'supports'      => array( 'title', 'thumbnail', 'excerpt', 'comments','editor','author' ),
		'has_archive'   => true,
		'exclude_from_search'   => false,
	);
	register_post_type( 'video', $args );	
}
add_action( 'init', 'video_custom_post' );

function video_updated_messages( $messages ) {
	global $post, $post_ID;
	$messages['video'] = array(
		0 => '', 
		1 => sprintf( __('تم التحديث. <a href="%s">عرض المقاله المرئيه</a>'), esc_url( get_permalink($post_ID) ) ),
		2 => __('تم تحديث العنصر.'),
		3 => __('تم حذف العنصر.'),
		4 => __('تم تحديث المقاله.'),
		5 => isset($_GET['revision']) ? sprintf( __('تم استعادة المقاله، من %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('تم النشر. <a href="%s">عرض المقاله المرئيه</a>'), esc_url( get_permalink($post_ID) ) ),
		7 => __('تم حفظ المرئيات.'),
		8 => sprintf( __('تم ارسال المقاله. <a target="_blank" href="%s">عرض المقاله المرئيه</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
		9 => sprintf( __('تم تأريخ المقاله للنشر: <strong>%1$s</strong>. <a target="_blank" href="%2$s">عرض الفتوى</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
		10 => sprintf( __('تم تحديث المقاله كمسوده. <a target="_blank" href="%s">عرض الفتوى</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
	);
	return $messages;
}
add_filter( 'video_post_updated_messages', 'video_updated_messages' );

function video_taxonomies() {
	$labels = array(
		'name'              => _x( 'تصنيفات المرئيات', 'taxonomy general name' ),
		'singular_name'     => _x( 'تصنيف المرئيات', 'taxonomy singular name' ),
		'search_items'      => __( 'البحث في تصنيفات المرئيات' ),
		'all_items'         => __( 'جميع تصنيفات المرئيات' ),
		'parent_item'       => __( 'التصنيف الأب لتصنيف المرئيات' ),
		'parent_item_colon' => __( 'التصنيف الأب لتصنيف المرئيات:' ),
		'edit_item'         => __( 'تعديل تصنيف المرئيات' ), 
		'update_item'       => __( 'تحديث تصنيف المرئيات' ),
		'add_new_item'      => __( 'إضافة تصنيف للمرئيات' ),
		'new_item_name'     => __( 'تصنيف جديد للمرئيات' ),
		'menu_name'         => __( 'تصنيفات المرئيات' ),
	);
	$args = array(
		'labels'				=> $labels,
		'hierarchical'			=> true,
		'public'				=> true,
		'show_ui'				=> true,
		'show_admin_column'		=> true,
		'show_in_nav_menus'		=> true,
		'show_tagcloud'			=> true,
	);
	register_taxonomy( 'video_category', 'video', $args );
}
add_action( 'init', 'video_taxonomies', 0 );

function add_video_meta_box() {  
    add_meta_box(  
        'video_meta_box', // $id  
        'رابط الملف للتحميل', // $title   
        'show_video_meta_box', // $callback  
        'video', // $page  
        'normal', // $context  
        'high'); // $priority  
}  
add_action('add_meta_boxes', 'add_video_meta_box'); 

$prefix = 'video_';  
$video_meta_fields = array(  
    array(  
        'label'=> 'رابط ا .mp4',  
        'desc'  => 'من فضلك قم بإضافة رابط الملف المرئى للتحميل',  
        'id'    => $prefix.'text',  
        'type'  => 'text'  
    ),  
    
	array(  
        'label'=> 'رابط Youtube',  
        'desc'  => 'من فضلك قم بإضافة رابط فيديو يوتيوب ID example: IwjVN3yguQQ',  
        'id'    => $prefix.'youtube',  
        'type'  => 'text'  
    ),  
	);

	// The Callback
function show_video_meta_box() {
global $video_meta_fields, $post;
// Use nonce for verification
echo '<input type="hidden" name="video_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';
	
	// Begin the field table and loop
	echo '<table class="form-table">';
	foreach ($video_meta_fields as $field) {
		// get value of this field if it exists for this post
		$meta = get_post_meta($post->ID, $field['id'], true);
		// begin a table row with
		echo '<tr>
				<th><label for="'.$field['id'].'">'.$field['label'].'</label></th>
				<td>';
				switch($field['type']) {
				// case items will go here
					// text
					case 'text':
						echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" />
							<br /><span class="description">'.$field['desc'].'</span>';
					break;	
				} //end switch
		echo '</td></tr>';
	} // end foreach
	echo '</table>'; // end table
}

// Save the Data
function save_video_meta($post_id) {
    global $video_meta_fields;
	
	// verify nonce
	if (!wp_verify_nonce($_POST['video_meta_box_nonce'], basename(__FILE__))) 
		return $post_id;
	// check autosave
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
		return $post_id;
	// check permissions
	if ('page' == $_POST['post_type']) {
		if (!current_user_can('edit_page', $post_id))
			return $post_id;
		} elseif (!current_user_can('edit_post', $post_id)) {
			return $post_id;
	}
	
	// loop through fields and save the data
	foreach ($video_meta_fields as $field) {
		$old = get_post_meta($post_id, $field['id'], true);
		$new = $_POST[$field['id']];
		if ($new && $new != $old) {
			update_post_meta($post_id, $field['id'], $new);
		} elseif ('' == $new && $old) {
			delete_post_meta($post_id, $field['id'], $old);
		}
	} // end foreach
}
add_action('save_post', 'save_video_meta');  
  
  

?>