<?php
$page = htmlentities($_GET['page']);

?>
<!DOCTYPE html>
<head>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script type="text/javascript" src="../../../../wp-includes/js/tinymce/tiny_mce_popup.js"></script>
	<link rel='stylesheet' href='shortcodes.css' type='text/css' media='all' />
<?php
if( $page == 'audio' ){ ?>

	<script type="text/javascript">
		var AddAudio = {
			e: '',
			init: function(e) {
				AddAudio.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {

				var mp3Url = jQuery('#mp3Url').val();
				var m4aUrl = jQuery('#m4aUrl').val();
				var oggUrl = jQuery('#oggUrl').val();

				var output = '[audio ';
				
				if(mp3Url) {
					output += 'mp3="'+mp3Url+'" ';
				}				
				if(m4aUrl) {
					output += 'm4a="'+m4aUrl+'" ';
				}				
				if(oggUrl) {
					output += 'ogg="'+oggUrl+'" ';
				}

				output += ']';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(AddAudio.init, AddAudio);

	</script>
	<title>Add Mp3 Player</title>

</head>
<body>
<form id="GalleryShortcode">
	<p>
		<label for="mp3Url">Mp3 file Url : </label>
		<input id="mp3Url" name="mp3Url" type="text" value="" />
	</p>
	<p>
		<label for="m4aUrl">M4A file Url : </label>
		<input id="m4aUrl" name="m4aUrl" type="text" value="" />
	</p>
	<p>
		<label for="oggUrl">OGG file Url : </label>
		<input id="oggUrl" name="oggUrl" type="text" value="" />
	</p>

	
	<p><a class="add" href="javascript:AddAudio.insert(AddAudio.e)">insert into post</a></p>
</form>

<?php } elseif( $page == 'video' ){ ?>

	<script type="text/javascript">
		var AddVideo = {
			e: '',
			init: function(e) {
				AddVideo.e = e;
				tinyMCEPopup.resizeToInnerSize();
			},
			insert: function createGalleryShortcode(e) {

				var VideoUrl = jQuery('#VideoUrl').val();
				var width = jQuery('#width').val();
				var height = jQuery('#height').val();


				var output = '[video ';
				
				if(width) {
					output += 'width="'+width+'" ';
				}
				if(height) {
					output += 'height="'+height+'" ';
				}

				output += ']'+ VideoUrl + '[/video]';
				tinyMCEPopup.execCommand('mceReplaceContent', false, output);
				tinyMCEPopup.close();
				
			}
		}
		tinyMCEPopup.onInit.add(AddVideo.init, AddVideo);

	</script>
	<title>Add Video</title>

</head>
<body>
<form id="GalleryShortcode">
	<p>
		<label for="VideoUrl">Youtube Url:</label>
		<input id="VideoUrl" name="VideoUrl" type="text" value="http://" />

	</p>
	<p>
		<label for="width">Width:</label>
		<input style="width:40px;" id="width" name="width" type="text" value="" />
	</p>
	<p>
		<label for="height">Height:</label>
		<input style="width:40px;"  id="height" name="height" type="text" value="" />
	</p>

	
	<p><a class="add" href="javascript:AddVideo.insert(AddVideo.e)">insert into post</a></p>
</form>


<?php } ?>

</body>
</html>