<?php
	// BOOKS
	$option_template01 = get_option('ahloman_template01');
	// 
	$option_template02 = get_option('ahloman_template02');
	// 
	$option_template03 = get_option('ahloman_template03');
	// 
	$option_template04 = get_option('ahloman_template04');
	// 
	$option_template05 = get_option('ahloman_template05');
	// 
	$option_template06 = get_option('ahloman_template06');
?>

<?php
if ( in_category($option_template01) ) {
	include(TEMPLATEPATH."/archive/template1.php");
} elseif ( in_category($option_template02) ) {
	include(TEMPLATEPATH."/archive/template2.php");
} elseif ( post_type_exists( 'fatawa' ) ) {
	include(TEMPLATEPATH."/archive/fatawa.php");
} elseif ( post_type_exists( 'books' ) ) {
	include(TEMPLATEPATH."/archive/books.php");
} elseif ( post_type_exists( 'estesharat' ) ) {
	include(TEMPLATEPATH."/archive/estesharat.php");
} else {
	// Continue with normal Loop
	include(TEMPLATEPATH."/archive/archive.php");
}
?>