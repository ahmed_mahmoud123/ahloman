<?php
/* 
 * Template Name: Fatwa	
 */
 get_header(); ?>
<?php
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$fatwaargs = array(
			'post_type' => 'fatawa',
			'posts_per_page' => '10'
		);
		$fatwa = new WP_Query( $fatwaargs );
		$_SESSION['a']="ahmed";
?>
<?php include(TEMPLATEPATH."/sidebar.php"); ?>
		<div class="topcontentright">
			<?php if(have_posts()): while(have_posts()): the_post() ?>
						<?php the_content() ?>
			<?php endwhile; endif; ?>
			<div class="box">
				<div class="boxtop">
					<div class="titlebg">
						<div class="title">
							<div class="icon icon11"></div> <?php wp_title(''); ?>
						</div>
					</div>
				</div>
				<div class="boxcenter">
					<div class="archivesinglelist">
						<ul>
							<li>
								<ul>
									<?php if( $fatwa->have_posts() ) {
											if(!isset($GLOBALS['$count'])){
										$GLOBALS['$count']=$fatwa->post_count-1;		
											}
										
										while( $fatwa->have_posts() ) {
										$fatwa->the_post(); 
										$fatawa_question = get_post_meta($post->ID, 'fatawa_question', true); 
										$fatawa_answer = get_post_meta($post->ID, 'fatawa_answer', true);	
										$_SESSION[$post->ID]=	$GLOBALS['$count']	;
									?>
									<li>
										<div class="title">
											<a href="<?php the_permalink(); ?>" ><h1><?php the_title() ?></h1></a>
											<span><p style="text-indent: 2px"><strong>المفتي: </strong><?php the_author(); ?></p> </span><br>
											<p>رقم الفتوى: <?php echo $GLOBALS['$count']; $GLOBALS['$count']=$GLOBALS['$count']-1; ?></p>
											<p><span style="color:#ff0000">السؤال:</span>
											<a href="<?php the_permalink(); ?>" ><?php echo $fatawa_question ?></a>
										</div>
										
										<div class="image"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php if ( has_post_thumbnail() ) { the_post_thumbnail( 'thumb75' ); } ?></a></div>
										<div class="date"><span><strong>تاريخ الإضافة:</strong> <?php the_time('d-m-Y'); ?></span><?php if(function_exists('the_views')) { ?><span><strong>زيارة:</strong> <?php the_views(); ?></span><?php } ?><span><strong>التعليقات:</strong> <?php comments_number( 'لا يوجد تعليقات', 'تعليق واحد', '%' ); ?></span><span><span><?php the_terms( $post->ID, 'fatawa_category', '<strong>التصنيفات: </strong>', ' / ' ); ?></span></div>
									</li>
									<?php
											}
										}
										else { ?>
											<li>
												<div class="title">
													<h1>عفوا</h1>
													<p>عفوا.. لايوجد كتب فى الوقت الحالى</p>
												</div>
												<div class="image"></div>
											</li>
										<?php
										}
									?>
								</ul>
							</li>
						</ul>
						<div class="navigation">
							<div class="alignright">
								<?php next_posts_link(' السابق ',$fatwa->max_num_pages) ?>
							</div>
							<div class="alignleft">
								<?php previous_posts_link(' التالي ') ?>
							</div>
						</div>
					</div>
				</div>
			</div>
					<div class="box most-post">
				<div class="boxtop">
					<div class="titlebg">
						<div class="title">
							<div class="icon icon11"></div>الأكثر مشاهدة
						</div>
					</div>
				</div>
			<div class="boxcenter">
			<div class="archivesinglelist">
						<ul>
							<li>
								<ul>
									<?php  
										$args = array(
													'post_type' => 'fatawa',
													'posts_per_page' => 3,
													'meta_key' => 'views',
													'orderby' => 'meta_value_num',
													'order' => 'DESC',
													); ?>
										<?php query_posts($args); ?>
										<?php if(have_posts() ) 
												{
													
												while(have_posts() ) 
												{
													the_post(); 
										$fatawa_question = get_post_meta($post->ID, 'fatawa_question', true); 
										$fatawa_answer = get_post_meta($post->ID, 'fatawa_answer', true);			
									?>
									<li>
										<div class="title">
											<a href="<?php the_permalink(); ?>" ><h1><?php the_title() ?></h1></a>
											<span><p style="text-indent: 2px"><strong>المفتي: </strong><?php the_author(); ?></p> </span><br>
											<p>رقم الفتوى: <?php echo taxonomy_seq($post->ID,'fatawa');  ?></p>
											<p><span style="color:#ff0000">السؤال:</span>
											<a href="<?php the_permalink(); ?>" ><?php echo $fatawa_question ?></a>
										</div>
										<div class="image"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php if ( has_post_thumbnail() ) { the_post_thumbnail( 'thumb75' ); } ?></a></div>
										<div class="date"><span><strong>تاريخ الإضافة:</strong> <?php the_time('d-m-Y'); ?></span><?php if(function_exists('the_views')) { ?><span><strong>زيارة:</strong> <?php the_views(); ?></span><?php } ?><span><strong>التعليقات:</strong> <?php comments_number( 'لا يوجد تعليقات', 'تعليق واحد', '%' ); ?></span><span><?php the_terms( $post->ID, 'fatawa_category', '<strong>التصنيفات: </strong>', ' / ' ); ?></span></div>
									</li>
									<?php
											}
										}
										else { ?>
											<li>
												<div class="title">
													
													<h1>عفوا</h1>
													<p>عفوا.. لايوجد صوتيات فى الوقت الحالى</p>
												</div>
												<div class="image"></div>
											</li>
										<?php
										}
									?>
								</ul>
							</li>
						</ul>
						
					</div>
				</div>
			</div>
			<div class="box most-post">
				<div class="boxtop">
					<div class="titlebg">
						<div class="title">
							<div class="icon icon11"></div>الأكثر تعليقاً
						</div>
					</div>
				</div>
			<div class="boxcenter">
			<div class="archivesinglelist">
						<ul>
							<li>
								<ul>
									<?php  
										$args = array(
													'post_type' => 'fatawa',
													'posts_per_page' => 3,
													'orderby' => 'comment_count',
													'order' => 'DESC',
													); ?>
										<?php query_posts($args); ?>
										<?php if(have_posts() ) 
												{
													
												while(have_posts() ) 
												{
													the_post(); 
										$fatawa_question = get_post_meta($post->ID, 'fatawa_question', true); 
										$fatawa_answer = get_post_meta($post->ID, 'fatawa_answer', true);			
									?>
									<li>
										<div class="title">
											<a href="<?php the_permalink(); ?>" ><h1><?php the_title() ?></h1></a>
											<span><p style="text-indent: 2px"><strong>المفتي: </strong><?php the_author(); ?></p> </span><br>
											<p>رقم الفتوى: <?php echo taxonomy_seq($post->ID,'fatawa');  ?></p>
											<p><span style="color:#ff0000">السؤال:</span>
											<a href="<?php the_permalink(); ?>" ><?php echo $fatawa_question ?></a>
										</div>
										<div class="image"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php if ( has_post_thumbnail() ) { the_post_thumbnail( 'thumb75' ); } ?></a></div>
										<div class="date"><span><strong>تاريخ الإضافة:</strong> <?php the_time('d-m-Y'); ?></span><?php if(function_exists('the_views')) { ?><span><strong>زيارة:</strong> <?php the_views(); ?></span><?php } ?><span><strong>التعليقات:</strong> <?php comments_number( 'لا يوجد تعليقات', 'تعليق واحد', '%' ); ?></span><span><?php the_terms( $post->ID, 'fatawa_category', '<strong>التصنيفات: </strong>', ' / ' ); ?></span></div>
									</li>
									<?php
											}
										}
										else { ?>
											<li>
												<div class="title">
													
													<h1>عفوا</h1>
													<p>عفوا.. لايوجد صوتيات فى الوقت الحالى</p>
												</div>
												<div class="image"></div>
											</li>
										<?php
										}
									?>
								</ul>
							</li>
						</ul>
						
					</div>
				</div>
			</div>
		</div>
<?php get_footer(); ?>