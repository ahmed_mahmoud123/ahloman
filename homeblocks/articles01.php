<?php
	$option_block1 = get_option('ahloman_block1');
	$option_block1_check = get_option('ahloman_block1_check');
?>

		<div class="mediumcontent">
		<?php if($option_block1_check == true) { ?>
			<div class="box">
				<div class="boxtop">
					<div class="titlebg">
						<div class="title">
							<div class="icon icon06"></div> <a href="<?php $block1 = get_category_link( $option_block1 ); echo $block1; ?>" alt="<?php get_cat_name( $option_block1 ); ?>" title="<?php get_cat_name( $option_block1 ); ?>"><?php echo get_cat_name($option_block1); ?></a>
						</div>
					</div>
				</div>
				<div class="boxcenter">
					<div class="articles">
						<div class="hiddenposition">
						<ul id="block01">
							<li class="list-container">
								<ul>
									<?php $linecount = 0; ?>
									<?php $recent = new WP_Query("post_type=post&showposts=16&orderby=last&cat=-10,-42,-62,-68,-12,-15"); while($recent->have_posts()) : $recent->the_post();?>
									<li>
										<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
										<div class="name"><?php the_author_posts_link(); ?></div>
									</li>
							<?php if ($linecount == 3) : ?>
								</ul>
							</li>
							<li class="list-container">
								<ul>
							<?php endif; ?>
							<?php if ($linecount == 7) : ?>
								</ul>
							</li>
							<li class="list-container">
								<ul>
							<?php endif; ?>
							<?php if ($linecount == 11) : ?>
								</ul>
							</li>
							<li class="list-container">
								<ul>
							<?php endif; ?>
							<?php if ($linecount == 15) : ?>
								</ul>
							</li>
							<li class="list-container">
								<ul>
							<?php endif; ?>
							<?php $linecount++; ?>
									<?php endwhile; ?>
								</ul>
							</li>
						</ul>
						</div>
						<div class="readmore"><a href="<?php $block1 = get_category_link( $option_block1 ); echo $block1; ?>" alt="<?php get_cat_name( $option_block1 ); ?>" title="<?php get_cat_name( $option_block1 ); ?>">اقرأ المزيد..</a></div>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>