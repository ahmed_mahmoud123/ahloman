<?php get_header(); ?>
<?php
	include(TEMPLATEPATH."/sidebar2.php");
	$terms_as_text = get_the_term_list( $post->ID, 'books_category') ;
?>
		<div class="topcontentright">
			<div class="box">
				<div class="boxtop">
					<div class="titlebg">
						<div class="title">
							<div class="icon icon01"></div><?php echo wp_title(); ?>
						</div>
					</div>
				</div>
				<div class="boxcenter">
					<div class="insidesinglepost">
					<h1 class="title"><?php wp_title(''); ?></h1>
					<?php if (have_posts()) : ?>
						<?php while (have_posts()) : the_post(); ?>
						<?php $val = get_post_meta($post->ID, 'book_file', true); ?>
						<?php if($val): ?>
						<h1><a target="_blank" href="<?php echo $val; ?>">تحميل الكتاب</a></h1>
						<?php endif; ?>
						<p><?php the_content(); ?></p>
						
						<?php endwhile; ?>
						<div class="share-btns">
							<span class='st_sharethis_hcount' displayText='ShareThis'></span>
							<span class='st_facebook_hcount' displayText='Facebook'></span>
							<span class='st_twitter_hcount' displayText='Tweet'></span>
							<span class='st_linkedin_hcount' displayText='LinkedIn'></span>
							<span class='st_pinterest_hcount' displayText='Pinterest'></span>
							<span class='st_email_hcount' displayText='Email'></span>
						</div>
					<?php endif; ?>
					<div class="spacerline"></div>	
					</div>
				</div>
				<?php comments_template(); ?>
			</div>
		</div>

<?php get_footer(); ?>