 <?php $args = array(
    'orderby'       => 'name', 
    'order'         => 'ASC', 
    'number'        => null,
    'optioncount'   => 1, 
    'exclude_admin' => true, 
    'show_fullname' => 1,
    'hide_empty'    => true,
    'echo'          => true,
    'style'         => 'list',
    'html'          => true ); 
	?> 
    
<?php
//Template Name: Authors
 get_header(); ?>
		
<?php include(TEMPLATEPATH."/sidebar.php"); ?>

		<div class="topcontentright">
			<div class="box">
				<div class="boxtop">
					<div class="titlebg">
						<div class="title">
							<div class="icon icon11"></div>الكتاب
						</div>
					</div>
				</div>
				<div class="boxcenter">
					<div class="archivesinglelist">
						<h2 class="authors-list-title">
							اسم الكاتب
							<span>
								عدد المواد
							</span>
						</h2>
						<ul class="authors-list">
							<?php wp_list_authors( $args ); ?>
						</ul>
						
						<div class="navigation">
							<div class="alignright">
								<?php next_posts_link(' السابق ') ?>
							</div>
							<div class="alignleft">
								<?php previous_posts_link(' التالي ') ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
<?php get_footer(); ?>