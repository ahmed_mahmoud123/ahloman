<?php
/* 
 * Template Name: Audio	
 */
 get_header(); ?>
<?php
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$audioargs = array(
			'post_type' => 'audio',
			'posts_per_page' => '10',
			'paged'=>$paged
		);
		$audio = new WP_Query( $audioargs );
?>
<?php include(TEMPLATEPATH."/sidebar.php"); ?>
		<div class="topcontentright">
			<div class="box">
				<div class="boxtop">
					<div class="titlebg">
						<div class="title">
							<div class="icon icon11"></div> <?php wp_title(''); ?>
						</div>
					</div>
				</div>
				<div class="boxcenter">
					<div class="archivesinglelist">
						<ul>
							<li>
								<ul>
									<?php if( $audio->have_posts() ) {
											while( $audio->have_posts() ) {
												$audio->the_post(); 
									$audio_text = get_post_meta($post->ID, 'audio_text', true); 			
									?>
									<li>
										<div class="title">
											<h1>- <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h1>
											<span><strong><p style="text-indent: 12px"><?php the_author_posts_link(); ?></p></strong> </span><br><br>
											<p><?php the_content_limit(400,''); ?></p>
										</div>
										<div class="image"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php if ( has_post_thumbnail() ) { the_post_thumbnail( 'thumb75' ); } ?></a></div>
										<div class="date"><span><strong>تاريخ الإضافة:</strong> <?php the_time('d-m-Y'); ?></span><?php if(function_exists('the_views')) { ?><span><strong>زيارة:</strong> <?php the_views(); ?></span><?php } ?><span><strong>التعليقات:</strong> <?php comments_number( 'لا يوجد تعليقات', 'تعليق واحد', '%' ); ?></span><?php the_terms( $post->ID, 'audio_category', '<strong>التصنيفات: </strong>', ' / ' ); ?></span></div>
									</li>
									<?php
											}
										}
										else { ?>
											<li>
												<div class="title">
													<h1>عفوا</h1>
													<p>عفوا.. لايوجد صوتيات فى الوقت الحالى</p>
												</div>
												<div class="image"></div>
											</li>
										<?php
										}
									?>
								</ul>
							</li>
						</ul>
						<div class="navigation">
							<div class="alignright">
								<?php next_posts_link(' السابق ',$audio->max_num_pages) ?>
							</div>
							<div class="alignleft">
								<?php previous_posts_link(' التالي ') ?>
							</div>
						</div>
					</div>
					
				</div>
			</div>
			<div class="box most-post">
				<div class="boxtop">
					<div class="titlebg">
						<div class="title">
							<div class="icon icon11"></div>الأكثر إستماع
						</div>
					</div>
				</div>
			<div class="boxcenter">
			<div class="archivesinglelist">
						<ul>
							<li>
								<ul>
									<?php  
										$args = array(
													'post_type' => 'audio',
													'posts_per_page' => 3,
													'meta_key' => 'views',
													'orderby' => 'meta_value_num',
													'order' => 'DESC',
													); ?>
										<?php query_posts($args); ?>
										<?php if(have_posts() ) 
												{
												while(have_posts() ) 
												{
													the_post(); 
										$audio_text = get_post_meta($post->ID, 'audio_text', true); 			
									?>
									<li> 
										<div class="title">
										<h1><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h1>
											
										</div>
										<div class="image"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php if ( has_post_thumbnail() ) { the_post_thumbnail( 'thumb75' ); } ?></a></div>
										<div class="date"><span><strong>تاريخ الإضافة:</strong> <?php the_time('d-m-Y'); ?></span><?php if(function_exists('the_views')) { ?><span><strong>زيارة:</strong> <?php the_views(); ?></span><?php } ?><span><strong>التعليقات:</strong> <?php comments_number( 'لا يوجد تعليقات', 'تعليق واحد', '%' ); ?></span><span><strong>بواسطة: </strong><?php the_author_posts_link(); ?> </span><?php the_terms( $post->ID, 'audio_category', '<strong>التصنيفات: </strong>', ' / ' ); ?></span></div>
									</li>
									<?php
											}
										}
										else { ?>
											<li>
												<div class="title">
													
													<h1>عفوا</h1>
													<p>عفوا.. لايوجد صوتيات فى الوقت الحالى</p>
												</div>
												<div class="image"></div>
											</li>
										<?php
										}
									?>
								</ul>
							</li>
						</ul>
						
					</div>
				</div>
			</div>
			<div class="box most-post">
				<div class="boxtop">
					<div class="titlebg">
						<div class="title">
							<div class="icon icon11"></div>الأكثر تعليق
						</div>
					</div>
				</div>
			<div class="boxcenter">
			<div class="archivesinglelist">
						<ul>
							<li>
								<ul>
									<?php  
										$args = array(
													'post_type' => 'audio',
													'posts_per_page' => 3,
													'orderby' => 'comment_count',
													'order' => 'DESC',
													); ?>
										<?php query_posts($args); ?>
										<?php if(have_posts() ) 
												{
												while(have_posts() ) 
												{
													the_post(); 
										$audio_text = get_post_meta($post->ID, 'audio_text', true); 			
									?>
									<li>
										<div class="title">
											<h1><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h1>
											
										</div>
										<div class="image"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php if ( has_post_thumbnail() ) { the_post_thumbnail( 'thumb75' ); } ?></a></div>
										<div class="date"><span><strong>تاريخ الإضافة:</strong> <?php the_time('d-m-Y'); ?></span><?php if(function_exists('the_views')) { ?><span><strong>زيارة:</strong> <?php the_views(); ?></span><?php } ?><span><strong>التعليقات:</strong> <?php comments_number( 'لا يوجد تعليقات', 'تعليق واحد', '%' ); ?></span><span><strong>بواسطة: </strong><?php the_author_posts_link(); ?> </span><?php the_terms( $post->ID, 'audio_category', '<strong>التصنيفات: </strong>', ' / ' ); ?></span></div>
									</li>
									<?php
											}
										}
										else { ?>
											<li>
												<div class="title">
													
													<h1>عفوا</h1>
													<p>عفوا.. لايوجد صوتيات فى الوقت الحالى</p>
												</div>
												<div class="image"></div>
											</li>
										<?php
										}
									?>
								</ul>
							</li>
						</ul>
						
					</div>
				</div>
			</div>
		</div>
<?php get_footer(); ?>