<?php $option_template0 = get_option('ahloman_template01'); ?>
		<div class="topcontentleft">
			<?php if ( in_category($option_template01) ) { ?>
			<div class="box">
				<div class="boxtop">
					<div class="titlebg">
						<div class="title">من تصنيف <a href="<?php $category1 = get_category_link( $option_template01 ); echo $category1; ?>" alt="<?php get_cat_name( $option_template01 ); ?>" title="<?php get_cat_name( $option_template01 ); ?>"><?php echo get_cat_name($option_template01); ?></a></div>
					</div>
				</div>
				<div class="boxcenter">
					<div class="blocksmall">
						<div class="insidesinglepostarticles">
							<ul> 
								<?php wp_list_categories('orderby=id&show_count=1&use_desc_for_title=0&title_li=&child_of='.$option_template01.''); ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="spacerline"></div>
			<?php } ?>
			<?php if (function_exists('get_most_viewed')): ?>
			<div class="box">
				<div class="boxtop">
					<div class="titlebg">
						<div class="title">الأكثر قراءة</div>
					</div>
				</div>
				<div class="boxcenter">
					<div class="blocksmall">
						<div class="insidesinglepostarticles">
							<ul>
								<?php get_most_viewed_category($option_template01 , 'post',10); ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="spacerline"></div>
			<?php endif; ?>
			<div class="box">
				<div class="boxtop">
					<div class="titlebg">
						<div class="title">الأكثر تعليقا</div>
					</div>
				</div>
				<div class="boxcenter">
					<div class="blocksmall">
						<div class="insidesinglepostarticles">
							<ul>
								<?php
								$cat_id = $option_template01;
								$args=array(
								  'cat' => $cat_id,
								  'orderby' => 'comment_count',
								  'order' => 'DESC',
								  'post_type' => 'post',
								  'post_status' => 'publish',
								  'posts_per_page' => 10,
								  'caller_get_posts'=> 1
								);
								$my_query = null;
								$my_query = new WP_Query($args);
								if( $my_query->have_posts() ) {
								  while ($my_query->have_posts()) : $my_query->the_post(); ?>
								    <li><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
								    <?php
								  endwhile;
								}else{ ?>
									<li>لا يوجد</li>
								<?php }
								wp_reset_query();  // Restore global post data stomped by the_post().
								?>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="spacerline"></div>
		</div>