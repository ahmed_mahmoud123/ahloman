<?php
	$option_mostread_check = get_option('ahloman_mostread_check');
	$option_mostread_day_check = get_option('ahloman_mostread_day_check');
	$option_mostread_month_check = get_option('ahloman_mostread_month_check');
	$option_mostread_year_check = get_option('ahloman_mostread_year_check');
	$option_mostread1 = get_option('ahloman_mostread1');
	$option_mostread1_check = get_option('ahloman_mostread1_check');
	$option_mostread2 = get_option('ahloman_mostread2');
	$option_mostread2_check = get_option('ahloman_mostread2_check');
	$option_mostread3 = get_option('ahloman_mostread3');
	$option_mostread3_check = get_option('ahloman_mostread3_check');
	$option_mostread4 = get_option('ahloman_mostread4');
	$option_mostread4_check = get_option('ahloman_mostread4_check');
	$option_mostread5 = get_option('ahloman_mostread5');
	$option_mostread5_check = get_option('ahloman_mostread5_check');
?>

<?php if($option_mostread_check == true) { ?>
			<div class="box">
				<div class="boxtop">
					<div class="titlebg">
						<div class="title">
							<div class="icon icon12"></div> الأكثر زيارة
						</div>
					</div>
				</div>
				<div class="boxcenter">
					<div id="timeanimator" class="mostread">
						<ul class="target">
							<?php if($option_mostread_day_check == true) { ?><li><a href="#timeanimatorday">يوم</a></li><?php } ?>
							<?php if($option_mostread_month_check == true) { ?><li><a href="#timeanimatormonth">شهر</a></li><?php } ?>
							<?php if($option_mostread_year_check == true) { ?><li><a href="#timeanimatoryear">سنة</a></li><?php } ?>
						</ul>
						
						<?php if($option_mostread_day_check == true) { ?>
						<div id="timeanimatorday" class="timeanimator">
							<div id="timeanimatordayblock">
								
								<?php if($option_mostread1_check == true) { ?>
								<div id="timeanimatorday-cat1">
								<div class="posts">
									<?php $recent = new WP_Query("post_type=video&showposts=5&orderby=last"); while($recent->have_posts()) : $recent->the_post();?>
									<div class="post"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></div>
									<?php endwhile; ?>
								</div>
								</div>
								<?php } ?>
								
								<?php if($option_mostread2_check == true) { ?>
								<div id="timeanimatorday-cat2">
								<div class="posts">
									<?php $recent = new WP_Query("post_type=audio&showposts=5&orderby=last"); while($recent->have_posts()) : $recent->the_post();?>
									<div class="post"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></div>
									<?php endwhile; ?>
								</div>
								</div>
								<?php } ?>
								
								<?php if($option_mostread3_check == true) { ?>
								<div id="timeanimatorday-cat3">
								<div class="posts">
									<?php $recent = new WP_Query("post_type=post&showposts=5&orderby=last"); while($recent->have_posts()) : $recent->the_post();?>
									<div class="post"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></div>
									<?php endwhile; ?>
								</div>
								</div>
								<?php } ?>
								
								<?php if($option_mostread4_check == true) { ?>
								<div id="timeanimatorday-cat4">
								<div class="posts">
									<?php $recent = new WP_Query("post_type=fatawa&showposts=5&orderby=last"); while($recent->have_posts()) : $recent->the_post();?>
									<div class="post"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></div>
									<?php endwhile; ?>
								</div>
								</div>
								<?php } ?>
								
								<?php if($option_mostread5_check == true) { ?>
								<div id="timeanimatorday-cat5">
								<div class="posts">
									<?php $recent = new WP_Query("post_type=estesharat&showposts=5&orderby=last"); while($recent->have_posts()) : $recent->the_post();?>
									<div class="post"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></div>
									<?php endwhile; ?>
								</div>
								</div>
								<?php } ?>
								
								<ul class="cats">
									<li><a href="#timeanimatorday-cat1">المرئيات</a></li>
									<?php if($option_mostread2_check == true) { ?><li><a href="#timeanimatorday-cat2">الصوتيات</a></li><?php } ?>
									<?php if($option_mostread3_check == true) { ?><li><a href="#timeanimatorday-cat3"><?php echo get_cat_name($option_mostread3); ?></a></li><?php } ?>
									<?php if($option_mostread4_check == true) { ?><li><a href="#timeanimatorday-cat4">الفتاوى و الأحكام</a></li><?php } ?>
									<?php if($option_mostread5_check == true) { ?><li><a href="#timeanimatorday-cat5"><?php echo get_cat_name($option_mostread5); ?></a></li><?php } ?>
								</ul>
							</div>
						</div>
						<?php } ?>
						
						
						
						
						
						
						
						<?php if($option_mostread_month_check == true) { ?>
						<div id="timeanimatormonth" class="timeanimator">
							<div id="timeanimatormonthblock">
								<?php if($option_mostread1_check == true) { ?>
								<div id="timeanimatormonth-cat1">
								<div class="posts">
									<?php
									  function filter_where( $where = '' ) {
                               $where .= " AND post_date > '" . date('Y-m-d', strtotime('-30 days')) . "'";
                               return $where;
                                                                           }
									   add_filter( 'posts_where', 'filter_where' );
									 $recent = new WP_Query("post_type=video&showposts=5&v_sortby=views&orderby=last"); while($recent->have_posts()) : $recent->the_post();?>
									<div class="post"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></div>
									<?php endwhile; 
									
									?>
								</div>
								</div>
								<?php } ?>
								
								<?php if($option_mostread2_check == true) { ?>
								<div id="timeanimatormonth-cat2">
								<div class="posts">
									<?php $recent = new WP_Query("post_type=audio&showposts=5&v_sortby=views&orderby=last"); while($recent->have_posts()) : $recent->the_post();?>
									<div class="post"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></div>
									<?php endwhile; ?>
								</div>
								</div>
								<?php } ?>
								
								<?php if($option_mostread3_check == true) { ?>
								<div id="timeanimatormonth-cat3">
								<div class="posts">
									<?php $recent = new WP_Query("post_type=post&showposts=5&v_sortby=views&orderby=last"); while($recent->have_posts()) : $recent->the_post();?>
									<div class="post"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></div>
									<?php endwhile; ?>
								</div>
								</div>
								<?php } ?>
								
								<?php if($option_mostread4_check == true) { ?>
								<div id="timeanimatormonth-cat4">
								<div class="posts">
									<?php $recent = new WP_Query("post_type=fatawa&showposts=5&v_sortby=views&orderby=last"); while($recent->have_posts()) : $recent->the_post();?>
									<div class="post"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></div>
									<?php endwhile; ?>
								</div>
								</div>
								<?php } ?>
								
								<?php if($option_mostread5_check == true) { ?>
								<div id="timeanimatormonth-cat5">
								<div class="posts">
									<?php $recent = new WP_Query("post_type=estesharat&showposts=5&v_sortby=views&orderby=last"); while($recent->have_posts()) : $recent->the_post();?>
									<div class="post"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></div>
									<?php endwhile; ?>
								</div>
								</div>
								<?php
								remove_filter( 'posts_where', 'filter_where' );
								 } ?>
								
								<ul class="cats">
									<?php if($option_mostread1_check == true) { ?><li><a href="#timeanimatormonth-cat1">المرئيات</a></li><?php } ?>
									<?php if($option_mostread2_check == true) { ?><li><a href="#timeanimatormonth-cat2">الصوتيات</a></li><?php } ?>
									<?php if($option_mostread3_check == true) { ?><li><a href="#timeanimatormonth-cat3"><?php echo get_cat_name($option_mostread3); ?></a></li><?php } ?>
									<?php if($option_mostread4_check == true) { ?><li><a href="#timeanimatormonth-cat4">الفتاوى والأحكام</a></li><?php } ?>
									<?php if($option_mostread5_check == true) { ?><li><a href="#timeanimatormonth-cat5"><?php echo get_cat_name($option_mostread5); ?></a></li><?php } ?>
								</ul>
							</div>
						</div>
						<?php } ?>
						
						
						
						
						
						
						
						<?php if($option_mostread_year_check == true) { ?>
						<div id="timeanimatoryear" class="timeanimator">
							<div id="timeanimatoryearblock">
								<?php if($option_mostread1_check == true) { ?>
								<div id="timeanimatoryear-cat1">
								<div class="posts">
									<?php 
									
									  function filter_where_year( $where = '' ) {
                               $where .= " AND post_date > '" . date('Y-m-d', strtotime('-365 days')) . "'";
                               return $where;
                                                                           }
									   add_filter( 'posts_where', 'filter_where_year' );
									$recent = new WP_Query("post_type=video&showposts=5&v_sortby=views&orderby=last"); while($recent->have_posts()) : $recent->the_post();?>
									<div class="post"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></div>
									<?php endwhile; ?>
								</div>
								</div>
								<?php } ?>
								
								<?php if($option_mostread2_check == true) { ?>
								<div id="timeanimatoryear-cat2">
								<div class="posts">
									<?php $recent = new WP_Query("post_type=audio&showposts=5&v_sortby=views&orderby=last"); while($recent->have_posts()) : $recent->the_post();?>
									<div class="post"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></div>
									<?php endwhile; ?>
								</div>
								</div>
								<?php } ?>
								
								<?php if($option_mostread3_check == true) { ?>
								<div id="timeanimatoryear-cat3">
								<div class="posts">
									<?php $recent = new WP_Query("post_type=post&showposts=5&v_sortby=views&orderby=last"); while($recent->have_posts()) : $recent->the_post();?>
									<div class="post"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></div>
									<?php endwhile; ?>
								</div>
								</div>
								<?php } ?>
								
								<?php if($option_mostread4_check == true) { ?>
								<div id="timeanimatoryear-cat4">
								<div class="posts">
									<?php $recent = new WP_Query("post_type=fatawa&showposts=5&v_sortby=views&orderby=last"); while($recent->have_posts()) : $recent->the_post();?>
									<div class="post"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></div>
									<?php endwhile; ?>
								</div>
								</div>
								<?php } ?>
								
								<?php if($option_mostread5_check == true) { ?>
								<div id="timeanimatoryear-cat5">
								<div class="posts">
									<?php $recent = new WP_Query("post_type=estesharat&showposts=5&v_sortby=views&orderby=last"); while($recent->have_posts()) : $recent->the_post();?>
									<div class="post"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></div>
									<?php endwhile; ?>
								</div>
								</div>
								<?php 
								remove_filter( 'posts_where', 'filter_where_year' );
								} ?>
								
								<ul class="cats">
									<?php if($option_mostread1_check == true) { ?><li><a href="#timeanimatoryear-cat1">المرئيات</a></li><?php } ?>
									<?php if($option_mostread2_check == true) { ?><li><a href="#timeanimatoryear-cat2">الصوتيات</a></li><?php } ?>
									<?php if($option_mostread3_check == true) { ?><li><a href="#timeanimatoryear-cat3"><?php echo get_cat_name($option_mostread3); ?></a></li><?php } ?>
									<?php if($option_mostread4_check == true) { ?><li><a href="#timeanimatoryear-cat4">الغتاوى والأحكام</a></li><?php } ?>
									<?php if($option_mostread5_check == true) { ?><li><a href="#timeanimatoryear-cat5"><?php echo get_cat_name($option_mostread5); ?></a></li><?php } ?>
								</ul>
							</div>
						</div>
						<?php } ?>
						
					</div>
				</div>
			</div>
<?php } ?>