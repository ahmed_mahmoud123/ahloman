<?php  
$themename = "الصفوة المختارة";
$shortname = "Safoa";

$options = array (
				
				array(	"name" => "البلوكات الرئيسية",
						"type" => "heading"),
						
				// headmaster block
				array(	"name" => "الكلمة الأسبوعية أو الشهرية",
			    		"id" => $shortname."_weaklyblock",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_weaklyblock_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
					array(	"type" => "breakline"),
							
				// Block 1
				array(	"name" => "البلوك رقم 1",
			    		"id" => $shortname."_block1",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_block1_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
				// Block 2
				array(	"name" => "البلوك رقم 2",
			    		"id" => $shortname."_block2",
			    		"type" => "posttype_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_block2_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
				// Block 3
				array(	"name" => "البلوك رقم 3",
			    		"id" => $shortname."_block3",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_block3_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
				array(	"name" => "البلوك اللذي يحتوي اكتر من تصنيف",
						"type" => "heading"),
						
				array(	"name" => "تفعيل البلوك الرئيسي",
			    		"id" => $shortname."_multicatblock_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
				// Block 4
				array(	"name" => "البلوك رقم 4",
			    		"id" => $shortname."_block4",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_block4_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
				// Block 5
				array(	"name" => "البلوك رقم 5",
			    		"id" => $shortname."_block5",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_block5_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
				// Block 6
				array(	"name" => "البلوك رقم 6",
			    		"id" => $shortname."_block6",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_block6_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
				
				
				// Block 8
				array(	"name" => "البلوك رقم 8",
			    		"id" => $shortname."_block8",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_block8_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
				array(	"name" => "المقاطع والبودكاست",
						"type" => "heading"),
				
				// Block 8
				array(	"name" => "مقاطع مختارة",
			    		"id" => $shortname."_block9",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_block9_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
				
				// Block 9
				array(	"name" => "البودكاست",
			    		"id" => $shortname."_block10",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_block10_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
			
				// Home Banner
				array(	"name" => "رابط الصوره الإعلانيه",
						"type" => "heading"),
						
				array(	"name" => "أدخل رابط للصوره الإعلانيه 650x70",
			    		"id" => $shortname."_footertext",
			    		"type" => "textarea"),
			    array(	"name" => "رابط صفحة الإعلان",
			    		"id" => $shortname."_ad_link",
			    		"type" => "textarea"),		
				array(	"type" => "breakline"),
                
                // 4 ads homepage
				array(	"name" => "الإعلانات المتجاوره",
						"type" => "heading"),
						
				array(	"name" => "صورة إعلان 1",
			    		"id" => $shortname."_ad1_image",
			    		"type" => "textarea"),
			    array(	"name" => "رابط صفحة الإعلان1",
			    		"id" => $shortname."_ad1_link",
			    		"type" => "textarea"),
                array(	"name" => "صورة إعلان2",
			    		"id" => $shortname."_ad2_image",
			    		"type" => "textarea"),
			    array(	"name" => "رابط صفحة الإعلان2",
			    		"id" => $shortname."_ad2_link",
			    		"type" => "textarea"),
                array(	"name" => "صورة إعلان3",
			    		"id" => $shortname."_ad3_image",
			    		"type" => "textarea"),
			    array(	"name" => "رابط صفحة الإعلان3",
			    		"id" => $shortname."_ad3_link",
			    		"type" => "textarea"),
                array(	"name" => "صورة إعلان4",
			    		"id" => $shortname."_ad4_image",
			    		"type" => "textarea"),
			    array(	"name" => "رابط صفحة الإعلان4",
			    		"id" => $shortname."_ad4_link",
			    		"type" => "textarea"),		
				array(	"type" => "breakline"),
);
		
function mytheme_add_admin() {

    global $themename, $shortname, $options;

    if ( $_GET['page'] == basename(__FILE__) ) {
    
        if ( 'save' == $_REQUEST['action'] ) {

                foreach ($options as $value) {
					if($value['type'] != 'multicheck'){
                    	update_option( $value['id'], $_REQUEST[ $value['id'] ] ); 
					}else{
						foreach($value['options'] as $mc_key => $mc_value){
							$up_opt = $value['id'].'_'.$mc_key;
							update_option($up_opt, $_REQUEST[$up_opt] );
						}
					}
				}
                foreach ($options as $value) {
					if($value['type'] != 'multicheck'){
                    	if( isset( $_REQUEST[ $value['id'] ] ) ) { update_option( $value['id'], $_REQUEST[ $value['id'] ]  ); } else { delete_option( $value['id'] ); } 
					}else{
						foreach($value['options'] as $mc_key => $mc_value){
							$up_opt = $value['id'].'_'.$mc_key;						
							if( isset( $_REQUEST[ $up_opt ] ) ) { update_option( $up_opt, $_REQUEST[ $up_opt ]  ); } else { delete_option( $up_opt ); } 
						}
					}
				}
                header("Location: themes.php?page=admin_panel.php&saved=true");
                die;

        } else if( 'reset' == $_REQUEST['action'] ) {

            foreach ($options as $value) {
				if($value['type'] != 'multicheck'){
                	delete_option( $value['id'] ); 
				}else{
					foreach($value['options'] as $mc_key => $mc_value){
						$del_opt = $value['id'].'_'.$mc_key;
						delete_option($del_opt);
					}
				}
			}
            header("Location: themes.php?page=admin_panel.php&reset=true");
            die;

        }
    }

    add_menu_page($themename."", "$themename", 'edit_themes', basename(__FILE__), 'mytheme_admin');

}

function mytheme_admin() {

    global $themename, $shortname, $options;

    if ( $_REQUEST['saved'] ) echo '<div id="message" class="updated fade"><p><strong>تم حفظ اعدادات '.$themename.' بنجاح.</strong></p></div>';
    
?>
<div class="wrap">
<style>
#mqheader{width:100%;height:120px;clear:both;border-bottom:3px solid #CECECE;
	background: -moz-linear-gradient(top, #F8F8F8 0%, #DBDBDB 100%);
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#F8F8F8), color-stop(100%,#DBDBDB));
	background: -webkit-linear-gradient(top, #F8F8F8 0%,#DBDBDB 100%);
	background: -o-linear-gradient(top, #F8F8F8 0%,#DBDBDB 100%);
	background: -ms-linear-gradient(top, #F8F8F8 0%,#DBDBDB 100%);
	background: linear-gradient(top, #F8F8F8 0%,#DBDBDB 100%);
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#F8F8F8', endColorstr='#DBDBDB',GradientType=0);
}
#mqheader .logo{width:320px;height:120px;margin:0;background:url(<?php bloginfo('stylesheet_directory'); ?>/images/sawa4logo.png) no-repeat top center;float:right;}
#mqheader .title{margin:40px 0 0 50px;font-size:30px;color:#153A7D;font-weight:bold;float:left;}
#mqfooter{width:100%;padding:10px;display:inline-table;background:url(<?php bloginfo('stylesheet_directory'); ?>/images/debut_dark.png) repeat top center;clear:both;color:#FFFFFF;}
#mqfooter a{color:#FFFFFF;text-decoration:normal;font-weight:bold;}
.spacerline h2{padding:0;}
</style>
<div class="spacerline"><h2 class="title"></h2></div>
<div id="mqheader">
	<h2 class="title">إعدادات <?php echo $themename; ?></h2>
	<div class="logo"></div>
</div>

<form method="post">

<table class="form-table">

<?php foreach ($options as $value) { 
	
	switch ( $value['type'] ) {
		case 'text':
		option_wrapper_header($value);
		?>
		        <input class="regular-text" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" value="<?php if ( get_settings( $value['id'] ) != "") { echo get_settings( $value['id'] ); } else { echo $value['std']; } ?>" />
		<?php
		option_wrapper_footer($value);
		break;
		
		case 'select':
		option_wrapper_header($value);
		?>
	            <select name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" style="width:25em;">
	                <?php foreach ($value['options'] as $option) { ?>
	                <option<?php if ( get_settings( $value['id'] ) == $option) { echo ' selected="selected"'; } elseif ($option == $value['std']) { echo ' selected="selected"'; } ?>><?php echo $option; ?></option>
	                <?php } ?>
	            </select>
		<?php
		option_wrapper_footer($value);
		break;
		
		//////////////////////////////////
		//This is the category select code
		//	Code courtesy of Nathan Rice
		case 'cat_select':
		option_wrapper_header($value);
		$categories = get_categories('hide_empty=0');
		?>
	            <select name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>"  style="width:25em;">
					<?php foreach ($categories as $cat) {
					if ( get_settings( $value['id'] ) == $cat->cat_ID) { $selected = ' selected="selected"'; } else { $selected = ''; }
					$opt = '<option value="' . $cat->cat_ID . '"' . $selected . '>' . $cat->cat_name . '</option>';
					echo $opt; } ?>
	            </select>
		<?php
		option_wrapper_footer($value);
		break;
		//end category select code
		//////////////////////////
		
		//////////////////////////////////
		//This is the taxonomy select code
		case 'posttype_select':
		option_wrapper_header($value);

		$args = array(
		   'public'   => true,
		   '_builtin' => false
		);

		$output = 'names'; // names or objects, note names is the default
		$operator = 'and'; // 'and' or 'or'

		$post_types = get_post_types( $args, $output, $operator ); 

		?>
	            <select name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>"  style="width:25em;">
					<?php foreach ( $post_types  as $post_type ) {
					if ( get_settings( $value['id'] ) == $post_type) { $selected = ' selected="selected"'; } else { $selected = ''; }
					$opt = '<option value="' . $post_type . '"' . $selected . '>' . $post_type . '</option>';
					echo $opt; } ?>
	            </select>
		<?php
		option_wrapper_footer($value);
		break;
		//end taxonomy select code
		//////////////////////////
		
		case 'textarea':
		$ta_options = $value['options'];
		option_wrapper_header($value);
		?>
				<textarea class="large-text" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>"><?php 
				if( get_settings($value['id']) != "") {
						echo stripslashes(get_settings($value['id']));
					}else{
						echo $value['std'];
				}?></textarea>
		<?php
		option_wrapper_footer($value);
		break;

		case "radio":
		option_wrapper_header($value);
		
 		foreach ($value['options'] as $key=>$option) { 
				$radio_setting = get_settings($value['id']);
				if($radio_setting != ''){
		    		if ($key == get_settings($value['id']) ) {
						$checked = "checked=\"checked\"";
						} else {
							$checked = "";
						}
				}else{
					if($key == $value['std']){
						$checked = "checked=\"checked\"";
					}else{
						$checked = "";
					}
				}?>
	            <input type="radio" name="<?php echo $value['id']; ?>" value="<?php echo $key; ?>" <?php echo $checked; ?> /><?php echo $option; ?><br />
		<?php 
		}
		 
		option_wrapper_footer($value);
		break;
		
		case "checkbox":
		option_wrapper_header($value);
						if(get_settings($value['id'])){
							$checked = "checked=\"checked\"";
						}else{
							$checked = "";
						}
					?>
		            <input type="checkbox" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" value="true" <?php echo $checked; ?> />
		<?php
		option_wrapper_footer($value);
		break;

		case "multicheck":
		option_wrapper_header($value);
		
 		foreach ($value['options'] as $key=>$option) {
	 			$pn_key = $value['id'] . '_' . $key;
				$checkbox_setting = get_settings($pn_key);
				if($checkbox_setting != ''){
		    		if (get_settings($pn_key) ) {
						$checked = "checked=\"checked\"";
						} else {
							$checked = "";
						}
				}else{
					if($key == $value['std']){
						$checked = "checked=\"checked\"";
					}else{
						$checked = "";
					}
				}?>
	            <input type="checkbox" name="<?php echo $pn_key; ?>" id="<?php echo $pn_key; ?>" value="true" <?php echo $checked; ?> /><label for="<?php echo $pn_key; ?>"><?php echo $option; ?></label><br />
		<?php 
		}
		 
		option_wrapper_footer($value);
		break;
		
		case "heading":
		?>
		<tr valign="top"> 
		    <td colspan="2" style="text-align: right;background:#153A7D;color:#FFFFFF;border-radius:10px;"><h3 style="margin:0;background:url(<?php bloginfo('stylesheet_directory'); ?>/images/icon02.png) no-repeat center right;padding-right:30px;"><?php echo $value['name']; ?></h3></td>
		</tr>
		<?php
		break;
		
		case "breakline":
		?>
		<tr valign="top"> 
		    <td colspan="2" style="text-align: right;height:2px;padding:0;margin:0;background:#ECECEC;"></td>
		</tr>
		<?php
		break;
		
		default:

		break;
	}
}
?>

<tr>
	<td></td>
</tr>

</table>

<p class="submit">
<input type="submit" name="save" class="button-primary" value="<?php _e('Save Changes') ?>" /> 
<input type="hidden" name="action" value="save" />
</p>
</form>

<?php
}

function option_wrapper_header($values){
	?>
	<tr valign="top"> 
	    <th scope="row" style="border-bottom:1px dashed #F5F5F5;"><?php echo $values['name']; ?>:</th>
	    <td style="border-bottom:1px dashed #F5F5F5;">
	<?php
}

function option_wrapper_footer($values){
	?>
	    </td>
	</tr>
	<?php 
}

function mytheme_wp_head() {
	$stylesheet = get_option('revmag_alt_stylesheet');
	if($stylesheet != ''){?>

<?php }
}

add_action('wp_head', 'mytheme_wp_head');
add_action('admin_menu', 'mytheme_add_admin'); 


?>