<?php
/**
 Template Name: Home
 */
 get_header(); 
 $option_ad1_image = get_option('ahloman_ad1_image');
 $option_ad1_link = get_option('ahloman_ad1_link');
 $option_ad2_image = get_option('ahloman_ad2_image');
 $option_ad2_link = get_option('ahloman_ad2_link');
 $option_ad3_image = get_option('ahloman_ad3_image');
 $option_ad3_link = get_option('ahloman_ad3_link');
 $option_ad4_image = get_option('ahloman_ad4_image');
 $option_ad4_link = get_option('ahloman_ad4_link');
 ?>
		<div class="topcontentleft">
			<?php include(TEMPLATEPATH."/homeblocks/headmaster.php"); ?>
			<div class="spacerline"></div>
			<?php include(TEMPLATEPATH."/homeblocks/maillist.php"); ?>
		</div>
		<div class="topcontentright">
			<?php include(TEMPLATEPATH."/homeblocks/weakly.php"); ?>
		</div>
	</div>
	<?php include(TEMPLATEPATH."/homeblocks/starredarticles.php"); ?>
	<div class="container">
		<?php include(TEMPLATEPATH."/homeblocks/media.php"); ?>
		<div class="spacerline"></div>
		<?php include(TEMPLATEPATH."/homeblocks/articles01.php"); ?>
		<?php include(TEMPLATEPATH."/homeblocks/articles02.php"); ?>
		<?php include(TEMPLATEPATH."/homeblocks/articles03.php"); ?>
		<div class="spacerline"></div>
		<div class="four-ads">
			<a href="<?php echo $option_ad1_link ?>">
				<img src="<?php echo $option_ad1_image ?>" alt="" />
			</a>
			<a href="<?php echo $option_ad2_link ?>">
				<img src="<?php echo $option_ad2_image ?>" alt="" />
			</a>
			<a href="<?php echo $option_ad3_link ?>">
				<img src="<?php echo $option_ad3_image ?>" alt="" />
			</a>
			<a href="<?php echo $option_ad4_link ?>">
				<img src="<?php echo $option_ad4_image ?>" alt="" />
			</a>
		</div>
		<div class="topcontentleft">
			<?php include(TEMPLATEPATH."/homeblocks/choosenmedia.php"); ?>
			<div class="spacerline"></div>
			<?php include(TEMPLATEPATH."/homeblocks/poodcast.php"); ?>
		</div>
		<div class="topcontentright">
			<?php include(TEMPLATEPATH."/homeblocks/changedbox.php"); ?>
			<div class="spacerline"></div>
			<?php include(TEMPLATEPATH."/homeblocks/books.php"); ?>
			<div class="spacerline"></div>
			<?php include(TEMPLATEPATH."/homeblocks/mostvisits.php"); ?>
		</div>
<?php get_footer(); ?>

