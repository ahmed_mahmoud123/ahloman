<?php get_header(); ?>

<?php include(TEMPLATEPATH."/sidebar2.php"); ?>
		<div class="topcontentright">
			<div class="box">
				<div class="boxtop">
					<div class="titlebg">
						<div class="title">
							<div class="icon icon01"></div> <?php wp_title(); ?>
						</div>
					</div>
				</div>
				<div class="boxcenter">
					<div class="insidesinglepost">
						<?php if (have_posts()) : ?>
							<?php while (have_posts()) : the_post(); ?>
							<p><?php the_content(); ?></p>
							<?php endwhile; ?>
						<?php else: ?>
							<p>لايوجد نتائج للبحث</p>
						<?php endif; ?>
					<div class="spacerline"></div>
					</div>
				</div>
			</div>
		</div>			
<?php get_footer(); ?>