<?php
	$option_block10 = get_option('ahloman_block10');
	$option_block10_check = get_option('ahloman_block10_check');
?>

<?php if($option_block10_check == true) { ?>
			<div class="box">
				<div class="boxtop">
					<div class="titlebg">
						<div class="title">
							<div class="icon icon11"></div> <a href="<?php $block10 = get_category_link( $option_block10 ); echo $block10; ?>" alt="<?php get_cat_name( $option_block10 ); ?>" title="<?php get_cat_name( $option_block10 ); ?>"><?php echo get_cat_name($option_block10); ?></a>
						</div>
					</div>
				</div>
				<div class="boxcenter">
					<div class="blocksmall">
						<div class="hiddenposition2">
						<ul id="block05">
							<li>
								<ul>
									<?php $recent = new WP_Query("cat=".$option_block10."&showposts=6&orderby=last"); while($recent->have_posts()) : $recent->the_post();?>
									<li>
										<div class="title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></div>
										<div class="image"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php if ( has_post_thumbnail() ) { the_post_thumbnail( 'thumb75' ); } ?></a></div>
									</li>
									<?php endwhile; ?>
								</ul>
							</li>
						</ul>
						</div>
					</div>
				</div>
			</div>
<?php } ?>