<?php get_header(); ?>

<?php
	include(TEMPLATEPATH."/sidebar2.php");
	$terms_as_text = get_the_term_list( $post->ID, 'books_category') ;
?>
		<div class="topcontentright">
			<div class="box">
				<div class="boxtop">
					<div class="titlebg">
						<div class="title">
							<div class="icon icon01"></div> <?php echo strip_tags($terms_as_text, ''); ?>
						</div>
					</div>
				</div>
				<div class="boxcenter">
					<div class="insidesinglepost">
					<h1 class="title"><?php wp_title(''); ?></h1>
					<?php if (have_posts()) : ?>
						<?php while (have_posts()) : the_post(); ?>
						<p><?php the_content(); ?></p>
						<?php endwhile; ?>
					<?php endif; ?>
					<div class="spacerline"></div>
					</div>
				</div>
			</div>
		</div>

			
<?php get_footer(); ?>