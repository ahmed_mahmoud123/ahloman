<!--DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="rtl" lang="ar"-->
<!DOCTYPE html >

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php bloginfo('name');  wp_title(' - ', true, 'left'); ?></title>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="text/xml" title="RSS .92" href="<?php bloginfo('rss_url'); ?>" />
<link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" /><?php wp_get_archives('type=monthly&format=link'); ?><?php //comments_popup_script(); // off by default ?><?php wp_head(); ?>

<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/reset.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/stylesheet.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/shortcodes.css" type="text/css" media="screen" />
<link href="<?php bloginfo('template_directory'); ?>/js/js-image-slider.css" rel="stylesheet" type="text/css" />
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.min-1.8.2.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery.bxslider.min.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/jquery-ui.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/CollapsibleLists.compressed.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/custom.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/mcVideoPlugin.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_directory'); ?>/js/js-image-slider.js" type="text/javascript"></script>

<script type="text/javascript">

      $(function(){ CollapsibleLists.applyTo(document.getElementById("test"),false); });
		
</script>


<style>
@media print {
   #topbar, #header, #latestbar, .topcontentleft, #_atssh, #footer, .addthis-smartlayers {
       display: none !important;
    }
    .topcontentright {
       display: block !important;
    }
}
</style>

</head>

<body dir="rtl">

<div id="wrapper">

	<div id="topbar">
		<div class="datebg">
    <div class="date"><span class="hijri"><?php echo date_i18n('D ') ?> <?php echo do_shortcode('[en_hijri_date]'); ?></span> <span class="georgian"><?php echo ' - '.date_i18n('d M Y'); ?></span></div>
		</div>
		<div class="content">
			<?php wp_nav_menu(array('theme_location' => 'topmenu', 'menu_class' => 'menu')); ?>
		</div>
	</div>
	
	<div id="header">
		<div class="leftcontent">
			<div class="search">
				<form method="get" id="searchform" action="<?php bloginfo('url'); ?>/">
					<input type="text" name="s" class="searchinput" value="أدخل ما تريد البحث عنه..." onfocus="if (this.value==this.defaultValue) this.value = ''" onblur="if (this.value=='') this.value = this.defaultValue" />
					<input type="submit" value="بحث" style="display:none;" name="submit">
					<span><a href="http://ahloman.net/?page_id=320">البحث المتقدم</a></span>
				</form>
			</div>
			<div class="menu">
				<?php wp_nav_menu(array('theme_location' => 'mainmenu', 'menu_class' => 'mainmenu')); ?>
			</div>
		</div>
		<div class="rightcontent">
			<div class="titles">
				<h3>المشرف العام فضيلة الشيخ</h3>
				<h2>أبي عبدالرحمن</h2>
				<h1>عبداللّه بن سالم سكرون</h1>
			</div>
			<div class="logo"></div>
		</div>
	</div>
	
	
	<div id="latestbar">
		<div class="content">
			
			<marquee width="100%"  behavior='' direction='right' hspace='0' vspace='0' scrolldelay='120' loop='' truespeed dir='ltr' onmouseover='this.stop()' onmouseout='this.start()'>
				<?php
                
                
                 $scroll = new WP_Query(array('showposts'=>'9','post_type'=>array('post','video','audio','fatawa','estesharat','book'))); ?>
					<?php if($scroll->have_posts()) : while($scroll->have_posts()): $scroll->the_post() ?>
						<a  dir="rtl" href="<?php the_permalink(); ?>"><?php 
						global $post;
					$title=get_the_title($post->ID);
					echo $title;
					$term_list = wp_get_post_terms($post->ID, 'fatawa_category', array("fields" => "names"));
					if(count($term_list)!=0){
						
						echo " [الفتاوى] ";						
					}
					
					$term_list = wp_get_post_terms($post->ID, 'video_category', array("fields" => "names"));
					if(count($term_list)!=0){
							
						echo " [المرئيات] ";
						
					}
					
					$term_list = wp_get_post_terms($post->ID, 'audio_category', array("fields" => "names"));
					if(count($term_list)!=0){
						
						echo " [الصوتيات] ";
						
					}
					
					$term_list = wp_get_post_terms($post->ID, 'book_category', array("fields" => "names"));
					if(count($term_list)!=0){
						
						echo " [الكتب] ";
						
					}
					
					$term_list = wp_get_post_terms($post->ID, 'estesharat_category', array("fields" => "names"));
					if(count($term_list)!=0){
							
						echo " [الأستشارات] ";
						
					}
					
					$term_list = wp_get_post_terms($post->ID, 'category', array("fields" => "names"));
					if(count($term_list)!=0){
						
						echo " [".$term_list[0]."]";
						
					}
						
						
						?></a>
					<?php echo "<span dir='rtl'>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	<span>"; endwhile; ?>
				<?php endif; ?>
			</marquee>
		</div>
		<div class="titlebg">
			
			<div class="title">جديد الموقع</div>
		</div>
	</div>
<?php if(is_front_page()){include(TEMPLATEPATH."/slider.php");}  ?>
	
	<div class="container">