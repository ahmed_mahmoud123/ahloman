<?php  
$themename = "أهل عمان";
$shortname = "ahloman";

$options = array (
	
				array(	"name" => "بلوكات الهيدر",
						"type" => "heading"),
				// Slider	
				array(	"name" => "السلايدر",
			    		"id" => $shortname."_slider",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_slider_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
				// headmaster articles
				array(	"name" => "مقالات المشرف العام",
			    		"id" => $shortname."_headmasterarticles",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_headmasterarticles_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
				// headmaster block
				array(	"name" => "خيارات بلوك المشرف العام",
						"type" => "heading"),
				array(	"name" => "القسم الرئيسي المشرف العام",
			    		"id" => $shortname."_headmasterarticlesblock",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_headmasterarticlesblock_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				array(	"name" => "تبويب 1",
			    		"id" => $shortname."_headmasterarticlesblock1",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_headmasterarticlesblock1_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				array(	"name" => "تبويب 2",
			    		"id" => $shortname."_headmasterarticlesblock2",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_headmasterarticlesblock2_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				array(	"name" => "تبويب 3",
			    		"id" => $shortname."_headmasterarticlesblock3",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_headmasterarticlesblock3_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				array(	"name" => "تبويب 4",
			    		"id" => $shortname."_headmasterarticlesblock4",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_headmasterarticlesblock4_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				array(	"name" => "تبويب 5",
			    		"id" => $shortname."_headmasterarticlesblock5",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_headmasterarticlesblock5_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
				
				array(	"name" => "البلوكات الرئيسية",
						"type" => "heading"),
						
				// headmaster block
				array(	"name" => "الكلمة الأسبوعية أو الشهرية",
			    		"id" => $shortname."_weaklyblock",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_weaklyblock_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
				// headmaster block
				array(	"name" => "المقالات المميزة",
			    		"id" => $shortname."_starredarticles",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_starredarticles_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
				array(	"name" => "الصوتيات والمرئيات",
						"type" => "heading"),
						
				array(	"name" => "تفعيل البلوك الرئيسي",
			    		"id" => $shortname."_mediablock_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
				// Vedio
				array(	"name" => "المرئيات",
			    		"id" => $shortname."_vedio",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_vedio_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
				// Vedio
				array(	"name" => "الصوتيات",
			    		"id" => $shortname."_media",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_media_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
				array(	"name" => "البلوكات الثلاثة المتجاورة",
						"type" => "heading"),
				
				// Block 1
				array(	"name" => "البلوك رقم 1",
			    		"id" => $shortname."_block1",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_block1_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
				// Block 2
				array(	"name" => "البلوك رقم 2",
			    		"id" => $shortname."_block2",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_block2_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
				// Block 3
				array(	"name" => "البلوك رقم 3",
			    		"id" => $shortname."_block3",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_block3_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
				array(	"name" => "البلوك اللذي يحتوي اكتر من تصنيف",
						"type" => "heading"),
						
				array(	"name" => "تفعيل البلوك الرئيسي",
			    		"id" => $shortname."_multicatblock_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
				// Block 4
				array(	"name" => "البلوك رقم 4",
			    		"id" => $shortname."_block4",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_block4_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
				// Block 5
				array(	"name" => "البلوك رقم 5",
			    		"id" => $shortname."_block5",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_block5_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
				// Block 6
				array(	"name" => "البلوك رقم 6",
			    		"id" => $shortname."_block6",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_block6_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
				// Block 7
				array(	"name" => "البلوك رقم 7",
			    		"id" => $shortname."_block7",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_block7_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
	
				array(	"name" => "خزانة الكتب",
						"type" => "heading"),
				
				// Block 8
				array(	"name" => "البلوك رقم 8",
			    		"id" => $shortname."_block8",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_block8_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
				array(	"name" => "المقاطع والبودكاست",
						"type" => "heading"),
				
				// Block 8
				array(	"name" => "مقاطع مختارة",
			    		"id" => $shortname."_block9",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_block9_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
				
				// Block 9
				array(	"name" => "البودكاست",
			    		"id" => $shortname."_block10",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_block10_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
				array(	"name" => "الأكثر زيارة",
						"type" => "heading"),
						
				array(	"name" => "تفعيل بلوك الأكثر زيارة",
			    		"id" => $shortname."_mostread_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
						
				array(	"name" => "تفعيل احصائيات يوم",
			    		"id" => $shortname."_mostread_day_check",
			    		"type" => "checkbox"),
				
				array(	"name" => "تفعيل احصائيات شهر",
			    		"id" => $shortname."_mostread_month_check",
			    		"type" => "checkbox"),
						
				array(	"name" => "تفعيل احصائيات سنة",
			    		"id" => $shortname."_mostread_year_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
						
				array(	"name" => "قسم 1",
			    		"id" => $shortname."_mostread1",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_mostread1_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
				array(	"name" => "قسم 2",
			    		"id" => $shortname."_mostread2",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_mostread2_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
				array(	"name" => "قسم 3",
			    		"id" => $shortname."_mostread3",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_mostread3_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
				array(	"name" => "قسم 4",
			    		"id" => $shortname."_mostread4",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_mostread4_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
				array(	"name" => "قسم 5",
			    		"id" => $shortname."_mostread5",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_mostread5_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
				array(	"name" => "تفعيل نظام قوالب الأقسام",
						"type" => "heading"),
				
				// template 01
				array(	"name" => "القالب 01",
			    		"id" => $shortname."_template01",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_template01_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
				// template 02
				array(	"name" => "القالب 02",
			    		"id" => $shortname."_template02",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_template02_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
				// template 03
				array(	"name" => "القالب 03",
			    		"id" => $shortname."_template03",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_template03_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
				// template 04
				array(	"name" => "القالب 04",
			    		"id" => $shortname."_template04",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_template04_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
				// template 05
				array(	"name" => "القالب 05",
			    		"id" => $shortname."_template05",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_template05_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
				// template 06
				array(	"name" => "القالب 06",
			    		"id" => $shortname."_template06",
			    		"type" => "cat_select"),
				array(	"name" => "تفعيل",
			    		"id" => $shortname."_template06_check",
			    		"type" => "checkbox"),
				array(	"type" => "breakline"),
				
				array(	"name" => "عن الموقع في الفوتر",
						"type" => "heading"),
						
				array(	"name" => "ادخل محتوى عن الموقع في الفوتر",
			    		"id" => $shortname."_footertext",
			    		"type" => "textarea"),
				array(	"type" => "breakline"),
);
		
function mytheme_add_admin() {

    global $themename, $shortname, $options;

    if ( $_GET['page'] == basename(__FILE__) ) {
    
        if ( 'save' == $_REQUEST['action'] ) {

                foreach ($options as $value) {
					if($value['type'] != 'multicheck'){
                    	update_option( $value['id'], $_REQUEST[ $value['id'] ] ); 
					}else{
						foreach($value['options'] as $mc_key => $mc_value){
							$up_opt = $value['id'].'_'.$mc_key;
							update_option($up_opt, $_REQUEST[$up_opt] );
						}
					}
				}

                foreach ($options as $value) {
					if($value['type'] != 'multicheck'){
                    	if( isset( $_REQUEST[ $value['id'] ] ) ) { update_option( $value['id'], $_REQUEST[ $value['id'] ]  ); } else { delete_option( $value['id'] ); } 
					}else{
						foreach($value['options'] as $mc_key => $mc_value){
							$up_opt = $value['id'].'_'.$mc_key;						
							if( isset( $_REQUEST[ $up_opt ] ) ) { update_option( $up_opt, $_REQUEST[ $up_opt ]  ); } else { delete_option( $up_opt ); } 
						}
					}
				}
                header("Location: themes.php?page=admin_panel.php&saved=true");
                die;

        } else if( 'reset' == $_REQUEST['action'] ) {

            foreach ($options as $value) {
				if($value['type'] != 'multicheck'){
                	delete_option( $value['id'] ); 
				}else{
					foreach($value['options'] as $mc_key => $mc_value){
						$del_opt = $value['id'].'_'.$mc_key;
						delete_option($del_opt);
					}
				}
			}
            header("Location: themes.php?page=admin_panel.php&reset=true");
            die;

        }
    }

    add_menu_page($themename."", "$themename", 'edit_themes', basename(__FILE__), 'mytheme_admin');

}

function mytheme_admin() {

    global $themename, $shortname, $options;

    if ( $_REQUEST['saved'] ) echo '<div id="message" class="updated fade"><p><strong>تم حفظ اعدادات '.$themename.' بنجاح.</strong></p></div>';
    
?>
<div class="wrap">
<div id="icon-options-general" class="icon32"><br></div><h2>إعدادات <?php echo $themename; ?></h2>

<form method="post">

<table class="form-table">

<?php foreach ($options as $value) { 
	
	switch ( $value['type'] ) {
		case 'text':
		option_wrapper_header($value);
		?>
		        <input class="regular-text" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" value="<?php if ( get_settings( $value['id'] ) != "") { echo get_settings( $value['id'] ); } else { echo $value['std']; } ?>" />
		<?php
		option_wrapper_footer($value);
		break;
		
		case 'select':
		option_wrapper_header($value);
		?>
	            <select name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" style="width:25em;">
	                <?php foreach ($value['options'] as $option) { ?>
	                <option<?php if ( get_settings( $value['id'] ) == $option) { echo ' selected="selected"'; } elseif ($option == $value['std']) { echo ' selected="selected"'; } ?>><?php echo $option; ?></option>
	                <?php } ?>
	            </select>
		<?php
		option_wrapper_footer($value);
		break;
		
		//////////////////////////////////
		//This is the category select code
		//	Code courtesy of Nathan Rice
		case 'cat_select':
		option_wrapper_header($value);
		$categories = get_categories('hide_empty=0');
		?>
	            <select name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>"  style="width:25em;">
					<?php foreach ($categories as $cat) {
					if ( get_settings( $value['id'] ) == $cat->cat_ID) { $selected = ' selected="selected"'; } else { $selected = ''; }
					$opt = '<option value="' . $cat->cat_ID . '"' . $selected . '>' . $cat->cat_name . '</option>';
					echo $opt; } ?>
	            </select>
		<?php
		option_wrapper_footer($value);
		break;
		//end category select code
		//////////////////////////
		
		case 'textarea':
		$ta_options = $value['options'];
		option_wrapper_header($value);
		?>
				<textarea class="large-text" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>"><?php 
				if( get_settings($value['id']) != "") {
						echo stripslashes(get_settings($value['id']));
					}else{
						echo $value['std'];
				}?></textarea>
		<?php
		option_wrapper_footer($value);
		break;

		case "radio":
		option_wrapper_header($value);
		
 		foreach ($value['options'] as $key=>$option) { 
				$radio_setting = get_settings($value['id']);
				if($radio_setting != ''){
		    		if ($key == get_settings($value['id']) ) {
						$checked = "checked=\"checked\"";
						} else {
							$checked = "";
						}
				}else{
					if($key == $value['std']){
						$checked = "checked=\"checked\"";
					}else{
						$checked = "";
					}
				}?>
	            <input type="radio" name="<?php echo $value['id']; ?>" value="<?php echo $key; ?>" <?php echo $checked; ?> /><?php echo $option; ?><br />
		<?php 
		}
		 
		option_wrapper_footer($value);
		break;
		
		case "checkbox":
		option_wrapper_header($value);
						if(get_settings($value['id'])){
							$checked = "checked=\"checked\"";
						}else{
							$checked = "";
						}
					?>
		            <input type="checkbox" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" value="true" <?php echo $checked; ?> />
		<?php
		option_wrapper_footer($value);
		break;

		case "multicheck":
		option_wrapper_header($value);
		
 		foreach ($value['options'] as $key=>$option) {
	 			$pn_key = $value['id'] . '_' . $key;
				$checkbox_setting = get_settings($pn_key);
				if($checkbox_setting != ''){
		    		if (get_settings($pn_key) ) {
						$checked = "checked=\"checked\"";
						} else {
							$checked = "";
						}
				}else{
					if($key == $value['std']){
						$checked = "checked=\"checked\"";
					}else{
						$checked = "";
					}
				}?>
	            <input type="checkbox" name="<?php echo $pn_key; ?>" id="<?php echo $pn_key; ?>" value="true" <?php echo $checked; ?> /><label for="<?php echo $pn_key; ?>"><?php echo $option; ?></label><br />
		<?php 
		}
		 
		option_wrapper_footer($value);
		break;
		
		case "heading":
		?>
		<tr valign="top"> 
		    <td colspan="2" style="text-align: right;background:#0071BC;color:#FFFFFF;"><h3 style="margin:0;"><?php echo $value['name']; ?></h3></td>
		</tr>
		<?php
		break;
		
		case "breakline":
		?>
		<tr valign="top"> 
		    <td colspan="2" style="text-align: right;height:2px;padding:0;margin:0;background:#ECECEC;"></td>
		</tr>
		<?php
		break;
		
		default:

		break;
	}
}
?>

</table>

<p class="submit">
<input type="submit" name="save" class="button-primary" value="<?php _e('Save Changes') ?>" /> 
<input type="hidden" name="action" value="save" />
</p>
</form>

<?php
}

function option_wrapper_header($values){
	?>
	<tr valign="top"> 
	    <th scope="row" style="border-bottom:1px dashed #F5F5F5;"><?php echo $values['name']; ?>:</th>
	    <td style="border-bottom:1px dashed #F5F5F5;">
	<?php
}

function option_wrapper_footer($values){
	?>
	    </td>
	</tr>
	<?php 
}

function mytheme_wp_head() {
	$stylesheet = get_option('revmag_alt_stylesheet');
	if($stylesheet != ''){?>

<?php }
}

add_action('wp_head', 'mytheme_wp_head');
add_action('admin_menu', 'mytheme_add_admin'); 



?>