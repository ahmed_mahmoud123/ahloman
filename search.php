<?php get_header(); ?>

<?php include(TEMPLATEPATH."/sidebar.php"); ?>
		<div class="topcontentright">
			<div class="box">
				<div class="boxtop">
					<div class="titlebg">
						<div class="title">
							<div class="icon icon11"></div> نتائج البحث عن: <?php the_search_query(); ?>
						</div>
					</div>
				</div>
				<div class="boxcenter">
					<div class="archivesinglelist">
						<ul>
							<li>
								<ul>
									<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
									<li>
										<div class="title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></div>
										<div class="image"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php if ( has_post_thumbnail() ) { the_post_thumbnail( 'thumb75' ); } ?></a></div>
									</li>
									<?php endwhile; ?>
									<?php else: ?>
										<p>لا يوجد نتائج للبحث, يمكنك المحاوله مره أخرى من القائمه الجانبيه</p>
									<?php endif; ?>
								</ul>
							</li>
						</ul>
						<div class="navigation">
							<div class="alignright">
								<?php next_posts_link(' السابق ') ?>
							</div>
							<div class="alignleft">
								<?php previous_posts_link(' التالي ') ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


			
<?php get_footer(); ?>