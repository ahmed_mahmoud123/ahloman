<?php get_header(); ?>
<?php
	include(TEMPLATEPATH."/sidebar2.php");
	$terms_as_text = get_the_term_list( $post->ID, 'video_category') ;
		
	//getting current author name
    if(isset($_GET['author_name'])) :

        $curauth = get_userdatabylogin($author_name);

    else :

        $curauth = get_userdata(intval($author));

    endif; 							
?>
		<div class="topcontentright">
			<div class="box">
				<div class="boxtop">
					<div class="titlebg">
						<div class="title">
							<div class="icon icon01"></div> <?php echo strip_tags($terms_as_text, ''); ?>
						</div>
					</div>
				</div>
				<div class="boxcenter">
					<div class="insidesinglepost">
						
						
					<h1 class="title"><?php wp_title(''); ?></h1>
					
					<?php if (have_posts()) : ?>
						<?php 
							while (have_posts()) : the_post(); 
							$video_text = get_post_meta($post->ID, 'video_text', true); 
							$video_youtube = get_post_meta($post->ID, 'video_youtube', true);
						 ?>
						<?php if(!$video_youtube && $video_text): ?>
						<video width="520" height="275" controls>
						  <source src="<?php echo $video_text; ?>" type="video/mp4">					  
							Your browser does not support the video tag.
						</video>
						<?php elseif($video_youtube && !$video_text): ?>
							<iframe width="520" height="275" src="http://www.youtube.com/embed/<?php echo $video_youtube; ?>" frameborder="0" allowfullscreen></iframe>
						<?php elseif($video_text && $video_youtube): ?>
							<iframe title="YouTube video player" class="youtube-player" type="text/html" width="520" height="275" src="http://www.youtube.com/embed/<?php echo $video_youtube; ?>" frameborder="0" allowFullScreen></iframe>
						<?php endif; ?>
						<p><?php the_content(); ?></p>
						<div class="file-info">
							<p>
								<span>إسم المحاضر:</span>
								<?php the_author_posts_link() ?>
							</p>
							<p>
								<span>تاريخ الاضافة:</span>
								 <?php the_time('d-m-Y'); ?>
							</p>
							<p>
								<span>التصنيف:</span>
								<?php the_terms( $post->ID, 'video_category', '', ' / ' ); ?>
							</p>
							<?php if(function_exists('the_views')) { ?><p><span>عدد الزيارات</span> <?php the_views(); ?></p><?php } ?>
						</div>
						<?php endwhile; ?>
						<div class="share-btns">
							<span class='st_sharethis_hcount' displayText='ShareThis'></span>
							<span class='st_facebook_hcount' displayText='Facebook'></span>
							<span class='st_twitter_hcount' displayText='Tweet'></span>
							<span class='st_linkedin_hcount' displayText='LinkedIn'></span>
							<span class='st_pinterest_hcount' displayText='Pinterest'></span>
							<span class='st_email_hcount' displayText='Email'></span>
						</div>
					<?php endif; ?>
					<div class="spacerline"></div>
					</div>
				</div>
				<?php comments_template(); ?>
			</div>
		</div>

			
<?php get_footer(); ?>