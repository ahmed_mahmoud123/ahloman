<?php get_header(); ?>
<!-- AddThis Smart Layers END -->
<?php
	include(TEMPLATEPATH."/sidebar2.php");
	$terms_as_text = get_the_term_list( $post->ID, 'fatawa_category') ;
	
?>
		<div class="topcontentright">
			<div class="box">
				<div class="boxtop">
					
					<div class="titlebg">
						
						<div class="title">
							<div class="icon icon01"></div> <?php echo strip_tags($terms_as_text, ''); ?>
						</div>
					</div>
				</div>
				<?php if (have_posts()) : ?>
					<?php while (have_posts()) : the_post(); 
						$fatawa_question = get_post_meta($post->ID, 'fatawa_question', true); 
						$fatawa_answer = get_post_meta($post->ID, 'fatawa_answer', true);
						$fatawa_audio = get_post_meta($post->ID, 'fatawa_audio', true);		
					?>
					
				<div class="boxcenter">
					<div class="insidesinglepost">
							
					<h1 class="title"><?php the_title() ?></h1>
					<br />
					
					<?php if($fatawa_audio): ?>
						<p>
							<audio controls>
								  <source src="<?php echo $fatawa_audio ?>" type="audio/mpeg">
								Your browser does not support the audio element.
							</audio>
						</p>
					<?php endif; ?>
					<p>رقم الفتوى: <?php echo taxonomy_seq($post->ID,'fatawa');  ?></p>
					
					<?php if($fatawa_question): ?>
					<p><span style="color:#ff0000">السؤال:</span></p>
						<p><?php echo $fatawa_question ?></p>
					<?php endif; ?>
					
					<?php if($fatawa_answer): ?>
					<p><span style="color:#0030ff">الجواب:</span></p>
						<?php echo $fatawa_answer; ?></p>
					<?php endif; ?>
					<?php if(function_exists('pf_show_link')){echo pf_show_link();} ?>
						<p><?php if(function_exists('the_views')) { ?><span><strong>عدد الزيارات: </strong> <?php the_views(); ?></span><?php } ?></p>
						<?php endwhile; ?>
						<div class="share-btns">
							<span class='st_sharethis_hcount' displayText='ShareThis'></span>
							<span class='st_facebook_hcount' displayText='Facebook'></span>
							<span class='st_twitter_hcount' displayText='Tweet'></span>
							<span class='st_linkedin_hcount' displayText='LinkedIn'></span>
							<span class='st_pinterest_hcount' displayText='Pinterest'></span>
							<span class='st_email_hcount' displayText='Email'></span>
						</div>
					<?php endif; ?>
					<div class="spacerline"></div>
					</div>
				</div>
				<?php comments_template(); ?>
			</div>
		</div>

			
<?php get_footer(); ?>