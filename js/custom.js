  $(function(){
  	$(".list-container").each(function(){
		var listContainer = $(this),
			targetUl = listContainer.children("ul"),
			targetLi = targetUl.children("li");
		if(targetLi.length == 0){
			listContainer.remove();
		}
	});
  });
  
  
  $(document).ready(function(){
  	
  	$("#expand").click(function(){
  		
		 $(function(){ CollapsibleLists.apply(true,'expand'); });
	});
  	
  	$("#collipse").click(function(){
  		
  		
  		if($("#collipse").html()=='عرض الجميع'){
  			$("#collipse").html('عرض الرئيسى');
  			 $(function(){ CollapsibleLists.applyTo(document.getElementById("test"),false,'expand'); });
  		}else{
  			$("#collipse").html('عرض الجميع');
  			 $(function(){ CollapsibleLists.applyTo(document.getElementById("test"),false,'close'); });
  		}
		
	});
    $('#top-slider-holder').bxSlider({
		auto: true,
		autoHover: true,
    	mode: 'vertical',
		pagerCustom: '#slider-nav'
	});
    $('#catheadmaster').bxSlider({
		mode: 'vertical',
		auto: true,
		pager: true
	});
    $('#block01').bxSlider({
		mode: 'vertical',
		auto: false,
		pager: true
	});
    $('#block02').bxSlider({
		mode: 'vertical',
		auto: false,
		pager: true
	});
	
	$('#block03').bxSlider({
		mode: 'vertical',
		auto: false,
		pager: true
	});
	$('#block04').bxSlider({
		mode: 'vertical',
		auto: false,
		pager: true
	});
	$('#block05').bxSlider({
		mode: 'vertical',
		auto: false,
		pager: true
	});
	$('#starredarticles').bxSlider({
		mode: 'fade',
		auto: false,
		pager: true,
		nextSelector: '#mqslidernext',
		prevSelector: '#mqsliderprev',
		nextText: ' ',
		prevText: ' '
	});
  });
  $(function() {
	$( "#tabs, #vedios, .video-tab, #timeanimator, #timeanimatordayblock, #timeanimatormonthblock, #timeanimatoryearblock" ).tabs();
	$( "#multicatblock, #multicatsubblock01, #multicatsubblock02, #multicatsubblock03" ).tabs();
  });
  
  // TopSlider tabs - auto play
  	$(function(){
	    /*setInterval(tabsAutoPlay, 5000);*/
	    /*var videoPlay = setInterval(videoTabsAutoPlay, 4500);
	    setInterval(videoTabsAutoPlay, 4500);
	    $(".vedio *").click(function(){
	    	clearInterval(videoPlay);
	    });*/
	    var targetTab = 0;
	    var targetVideo = 0;
	    function tabsAutoPlay(){
	    	var itemSize = $("#topslider .ui-tabs-nav li").length;
	  		$("#topslider .ui-tabs-nav li:eq("+targetTab+")").children("a").click();
	  		targetTab++;
		    if (targetTab == itemSize) {
		        targetTab = 0;
		    }
	  	}
	    function videoTabsAutoPlay(){
	    	var videoSize = $("#vedios .ui-tabs-nav li").length;
	  		$("#vedios .ui-tabs-nav li:eq("+targetVideo+")").find("a").click();
	  		targetVideo++;
		    if (targetVideo == videoSize) {
		        targetVideo = 0;
		    }
	  	}
	    function videoTabsAutoPlay2(){
	    	var videoSize = $(".videos-tab .ui-tabs-nav li").length;
	  		$(".videos-tab .ui-tabs-nav li:eq("+targetVideo+")").find("a").click();
	  		targetVideo++;
		    if (targetVideo == videoSize) {
		        targetVideo = 0;
		    }
	  	}
	});
  
	jQuery('.repeatable-add').click(function() {
	field = jQuery(this).closest('td').find('.custom_repeatable li:last').clone(true);
	fieldLocation = jQuery(this).closest('td').find('.custom_repeatable li:last');
	jQuery('input', field).val('').attr('name', function(index, name) {
		return name.replace(/(\d+)/, function(fullMatch, n) {
			return Number(n) + 1;
		});
	})
	field.insertAfter(fieldLocation, jQuery(this).closest('td'))
	return false;
});

jQuery('.repeatable-remove').click(function(){
	jQuery(this).parent().remove();
	return false;
});
	
jQuery('.custom_repeatable').sortable({
	opacity: 0.6,
	revert: true,
	cursor: 'move',
	handle: '.sort'
});

var currTab = 0;

// count of all tabs
var totalTabs = $("#titles > a").length;

// function to pass to setInterval
function cycle() {

    // simulate click on current tab
    $("#titles > a").eq(currTab).click();

    // increment counter   
    currTab++;

    // reset if we're at the last one
    if (currTab == totalTabs) {
        currTab = 0;
    }
}

// go!
var i = setInterval(cycle, 1000);
