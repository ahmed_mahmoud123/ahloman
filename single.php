<?php get_header(); ?>

<?php      
	//getting current author name
    if(isset($_GET['author_name'])) :

        $curauth = get_userdatabylogin($author_name);

    else :

        $curauth = get_userdata(intval($author));

    endif;   
?>

<?php include(TEMPLATEPATH."/sidebar2.php"); ?>

		<div class="topcontentright">
			<div class="box">
				<div class="boxtop">
					<div class="titlebg">
						<div class="title">
							<div class="icon icon01"></div> <?php wp_title(); ?>
						</div>
					</div>
				</div>
				<div class="boxcenter">
					<div class="insidesinglepost">
					<?php if (have_posts()) : ?>
						<?php while (have_posts()) : the_post(); ?>
							
							
						<div class="file-info author-box post-info">
							<?php echo get_avatar( get_the_author_meta('user_email', $curauth->id ), 100 ); ?>
							<div class="author-informations">
								<p>
									<span>عنوان المقال:</span>
									<?php wp_title(); ?>
								</p>
								<p> 
									<span>الكاتب:</span>
									<?php the_author_posts_link() ?>
								</p>
								<p>
									<span>تاريخ الاضافة:</span>
									 <?php the_time('d-m-Y'); ?>
								</p>
								<p>
									<span>التصنيف:</span>
									<?php the_category(' , '); ?>
								</p>
								<?php if(function_exists('the_views')) { ?><p><span>عدد الزيارات</span> <?php the_views(); ?></p><?php } ?>
							</div>
						</div>
						
						<div class="share-btns">
							<span class='st_sharethis_hcount' displayText='ShareThis'></span>
							<span class='st_facebook_hcount' displayText='Facebook'></span>
							<span class='st_twitter_hcount' displayText='Tweet'></span>
							<span class='st_linkedin_hcount' displayText='LinkedIn'></span>
							<span class='st_pinterest_hcount' displayText='Pinterest'></span>
							<span class='st_email_hcount' displayText='Email'></span>
						</div>
						
						<!--div class="date"><span><strong>تاريخ الإضافة:</strong> <?php the_time('d-m-Y'); ?></span><?php if(function_exists('the_views')) { ?><span><strong>زيارة:</strong> <?php the_views(); ?></span><?php } ?><span><strong>التعليقات:</strong> <?php comments_number( 'لا يوجد تعليقات', 'تعليق واحد', '%' ); ?></span><span><strong>التصنيف: </strong>المقالات</span><strong>الكاتب :</strong><?php the_author_posts_link() ?><span></span></div-->
							
						<p><?php the_content(); ?></p>
						<?php endwhile; ?>
						<?php if(function_exists('pf_show_link')){echo pf_show_link();} ?>
					<?php endif; ?>
					<div class="spacerline"></div>
					</div>
				</div>
				<?php comments_template(); ?>
			</div>
		</div>

			
<?php get_footer(); ?>