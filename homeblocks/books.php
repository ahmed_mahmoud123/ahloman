
			<div class="box">
				<div class="boxtop">
					<div class="titlebg">
						<div class="title">
							<div class="icon icon10"></div> <a href="http://ahloman.net/?page_id=330" alt="" title=""> خزانة الكتب </a>
						</div>
					</div>
				</div>
				<div class="boxcenter">
					<div class="liberary">
						<ul>
							<?php $books = new WP_Query(array('post_type'=>'books','posts_per_page'=>4)); while($books->have_posts()) : $books->the_post();?>
							<?php $val = get_post_meta($post->ID, 'book_author', true); ?>
							<li>
								<div class="title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></div>
								<div class="image"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php if ( has_post_thumbnail() ) { the_post_thumbnail( 'thumb80' ); } ?></a></div>
								<small class="title"><?php echo $val; ?></small>
							</li>
							<?php endwhile; ?>
						</ul>
						<div class="readmore"><a href="http://ahloman.net/?page_id=330" >تصفّح الخزانة</a></div>
					</div>
				</div>
			</div>