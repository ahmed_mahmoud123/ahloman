<?php

function estesharat_custom_post() {
	$labels = array(
		'name'               => _x( 'استشارات', 'post type general name' ),
		'singular_name'      => _x( 'استشارات', 'post type singular name' ),
		'add_new'            => _x( 'أضف جديد', 'estesharat' ),
		'add_new_item'       => __( 'أضف إستشاره جديدة' ),
		'edit_item'          => __( 'تعديل إستشاره' ),
		'new_item'           => __( 'إستشاره جديدة' ),
		'all_items'          => __( 'كل استشارات' ),
		'view_item'          => __( 'عرض الإستشاره' ),
		'search_items'       => __( 'البحث في استشارات' ),
		'not_found'          => __( 'لا يوجد استشارات' ),
		'not_found_in_trash' => __( 'لا يوجد استشارات في سلة المهملات' ), 
		'parent_item_colon'  => '',
		'menu_name'          => 'استشارات'
	);
	$args = array(
		'labels'        => $labels,
		'description'   => '',
		'public'        => true,
		'menu_position' => 5,
		'supports'      => array( 'title', 'thumbnail', 'excerpt', 'comments','author' ),
		'has_archive'   => true,
		'exclude_from_search'   => false,
	);
	register_post_type( 'estesharat', $args );	
}
add_action( 'init', 'estesharat_custom_post' );

function estesharat_updated_messages( $messages ) {
	global $post, $post_ID;
	$messages['estesharat'] = array(
		0 => '', 
		1 => sprintf( __('تم التحديث. <a href="%s">عرض الإستشاره</a>'), esc_url( get_permalink($post_ID) ) ),
		2 => __('تم تحديث العنصر.'),
		3 => __('تم حذف العنصر.'),
		4 => __('تم تحديث الإستشاره.'),
		5 => isset($_GET['revision']) ? sprintf( __('تم استعادة الإستشاره، من %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('تم النشر. <a href="%s">عرض الإستشاره</a>'), esc_url( get_permalink($post_ID) ) ),
		7 => __('تم حفظ الإستشاره.'),
		8 => sprintf( __('تم ارسال الإستشاره. <a target="_blank" href="%s">عرض الإستشاره</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
		9 => sprintf( __('تم تأريخ الإستشاره للنشر: <strong>%1$s</strong>. <a target="_blank" href="%2$s">عرض الإستشاره</a>'), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
		10 => sprintf( __('تم تحديث الإستشاره كمسوده. <a target="_blank" href="%s">عرض الإستشاره</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
	);
	return $messages;
}
add_filter( 'estesharat_post_updated_messages', 'estesharat_updated_messages' );

function estesharat_taxonomies() {
	$labels = array(
		'name'              => _x( 'تصنيفات استشارات', 'taxonomy general name' ),
		'singular_name'     => _x( 'تصنيف استشارات', 'taxonomy singular name' ),
		'search_items'      => __( 'البحث في تصنيفات استشارات' ),
		'all_items'         => __( 'جميع تصنيفات استشارات' ),
		'parent_item'       => __( 'التصنيف الأب لتصنيف استشارات' ),
		'parent_item_colon' => __( 'التصنيف الأب لتصنيف استشارات:' ),
		'edit_item'         => __( 'تعديل تصنيف استشارات' ), 
		'update_item'       => __( 'تحديث تصنيف استشارات' ),
		'add_new_item'      => __( 'إضافة تصنيف للاستشارات' ),
		'new_item_name'     => __( 'تصنيف جديد للاستشارات' ),
		'menu_name'         => __( 'تصنيفات استشارات' ),
	);
	$args = array(
		'labels'				=> $labels,
		'hierarchical'			=> true,
		'public'				=> true,
		'show_ui'				=> true,
		'show_admin_column'		=> true,
		'show_in_nav_menus'		=> true,
		'show_tagcloud'			=> true,
	);
	register_taxonomy( 'estesharat_category', 'estesharat', $args );
}
add_action( 'init', 'estesharat_taxonomies', 0 );

function add_esteshara_meta_box() {  
    add_meta_box(  
        'esteshara_meta_box', // $id  
        'محتوى الإستشاره', // $title   
        'show_esteshara_meta_box', // $callback  
        'estesharat', // $page  
        'normal', // $context  
        'high'); // $priority  
}  
add_action('add_meta_boxes', 'add_esteshara_meta_box'); 

$prefix = 'esteshara_';  
$esteshara_meta_fields = array(  
	    array(  
	        'label'=> 'السؤال',  
	        'desc'  => 'سؤال الإستشاره',  
	        'id'    => $prefix.'question',  
	        'type'  => 'textarea'  
	    ),  
		
		
		array(  
	        'label'=> 'الجواب',  
	        'desc'  => 'جواب االإستشاره',  
	        'id'    => $prefix.'answer',  
	        'type'  => 'textarea'  
	    ),  
	    
		array(  
	        'label'=> 'الإستشاره الصوتيه',  
	        'desc'  => 'رابط االإستشاره الصوتيه',  
	        'id'    => $prefix.'audio',  
	        'type'  => 'text'  
	    ), 
	);

	// The Callback
function show_esteshara_meta_box() {
global $esteshara_meta_fields, $post;
// Use nonce for verification
echo '<input type="hidden" name="esteshara_meta_box_nonce" value="'.wp_create_nonce(basename(__FILE__)).'" />';
	
	// Begin the field table and loop
	echo '<table class="form-table">';
	foreach ($esteshara_meta_fields as $field) {
		// get value of this field if it exists for this post
		$meta = get_post_meta($post->ID, $field['id'], true);
		// begin a table row with
		echo '<tr>
				<th><label for="'.$field['id'].'">'.$field['label'].'</label></th>
				<td>';
				switch($field['type']) {
				// case items will go here
					// text
					case 'text':
						echo '<input type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$meta.'" size="30" />
							<br /><span class="description">'.$field['desc'].'</span>';
					break;	
					
					// textarea  
					case 'textarea':  
					   wp_editor( $meta, $field['id'], array( 'textarea_name' => $field['id'], 'teeny' => FALSE ) ); 
					break; 
				} //end switch
		echo '</td></tr>';
	} // end foreach
	echo '</table>'; // end table
}

// Save the Data
function save_esteshara_meta($post_id) {
    global $esteshara_meta_fields;
	
	// verify nonce
	if (!wp_verify_nonce($_POST['esteshara_meta_box_nonce'], basename(__FILE__))) 
		return $post_id;
	// check autosave
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
		return $post_id;
	// check permissions
	if ('page' == $_POST['post_type']) {
		if (!current_user_can('edit_page', $post_id))
			return $post_id;
		} elseif (!current_user_can('edit_post', $post_id)) {
			return $post_id;
	}
	
	// loop through fields and save the data
	foreach ($esteshara_meta_fields as $field) {
		$old = get_post_meta($post_id, $field['id'], true);
		$new = $_POST[$field['id']];
		if ($new && $new != $old) {
			update_post_meta($post_id, $field['id'], $new);
		} elseif ('' == $new && $old) {
			delete_post_meta($post_id, $field['id'], $old);
		}
	} // end foreach
}
add_action('save_post', 'save_esteshara_meta');  
  


?>
