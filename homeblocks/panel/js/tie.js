jQuery(document).ready(function() {

    jQuery('.tooltip').tipsy({fade: true, gravity: 's'});
	
			
// image Uploader Functions ##############################################
	function tie_set_uploader(field) {
		var button = "#upload_"+field+"_button";
		jQuery(button).click(function() {
			window.restore_send_to_editor = window.send_to_editor;
			tb_show('', 'media-upload.php?referer=tie-settings&amp;type=image&amp;TB_iframe=true&amp;post_id=0');
			tie_set_send_img(field);
			return false;
		});
		jQuery('#'+field).change(function(){
			jQuery('#'+field+'-preview').show();
			jQuery('#'+field+'-preview img').attr("src",jQuery('#'+field).val());
		});
	}
	function tie_set_send_img(field) {
		window.send_to_editor = function(html) {
			imgurl = jQuery('img',html).attr('src');
			
			if(typeof imgurl == 'undefined') // Bug fix By Fouad Badawy
				imgurl = jQuery(html).attr('src');
				
			jQuery('#'+field).val(imgurl);
			jQuery('#'+field+'-preview').show();
			jQuery('#'+field+'-preview img').attr("src",imgurl);
			tb_remove();
			window.send_to_editor = window.restore_send_to_editor;
		}
	};
	
	tie_set_uploader("logo");
	tie_set_uploader("favicon");
	tie_set_uploader("gravatar");
	tie_set_uploader("banner_top_img");
	tie_set_uploader("banner_bottom_img");
	tie_set_uploader("banner_above_img");
	tie_set_uploader("banner_below_img");
	tie_set_uploader("dashboard_logo");

	
// image Uploader Functions ##############################################
	function tie_styling_uploader(field) {
		var button = "#upload_"+field+"_button";
		jQuery(button).click(function() {
			window.restore_send_to_editor = window.send_to_editor;
			tb_show('', 'media-upload.php?referer=tie-settings&amp;type=image&amp;TB_iframe=true&amp;post_id=0');
			styling_send_img(field);
			return false;
		});
		jQuery('#'+field).change(function(){
			jQuery('#'+field+'-preview img').attr("src",jQuery('#'+field).val());
		});
	}
	function styling_send_img(field) {
		window.send_to_editor = function(html) {
			imgurl = jQuery('img',html).attr('src');
			
			if(typeof imgurl == 'undefined') // Bug fix By Fouad Badawy
				imgurl = jQuery(html).attr('src');
				
			jQuery('#'+field+'-img').val(imgurl);
			jQuery('#'+field+'-preview').show();
			jQuery('#'+field+'-preview img').attr("src",imgurl);
			tb_remove();
			window.send_to_editor = window.restore_send_to_editor;
		}
	};	
	tie_styling_uploader("background");
	tie_styling_uploader("topbar_background");
	tie_styling_uploader("header_background");
	tie_styling_uploader("footer_background");

	
// Del Preview Image ##############################################
	jQuery(".del-img").live("click" , function() {
		jQuery(this).parent().fadeOut(function() {
			jQuery(this).hide();
			jQuery(this).parent().find('input[class="img-path"]').attr('value', '' );
		});
	});	
	

// Breaking News options ##############################################
	var selected_breaking = jQuery("input[name='tie_options[breaking_type]']:checked").val();
	
	if (selected_breaking == 'category') {jQuery('#breaking_cat-item').show();}
	if (selected_breaking == 'tag') {jQuery('#breaking_tag-item').show();}

	jQuery("input[name='tie_options[breaking_type]']").change(function(){
		var selected_breaking = jQuery("input[name='tie_options[breaking_type]']:checked").val();
		if (selected_breaking == 'category') {
			jQuery('#breaking_tag-item').hide();
			jQuery('#breaking_cat-item').fadeIn();
		}
		if (selected_breaking == 'tag') {
			jQuery('#breaking_cat-item').hide();
			jQuery('#breaking_tag-item').fadeIn();
		}

	 });
	 

	 
// Single Post Head ##############################################
	var selected_item = jQuery("select[name='tie_post_head'] option:selected").val();
	
	if (selected_item == 'video') {jQuery('#tie_video_url-item, #tie_embed_code-item').show();}
	if (selected_item == 'slider') {jQuery('#tie_post_slider-item').show();}
	if (selected_item == 'map') {jQuery('#tie_googlemap_url-item').show();}
	
	jQuery("select[name='tie_post_head']").change(function(){
		var selected_item = jQuery("select[name='tie_post_head'] option:selected").val();
		if (selected_item == 'video') {
			jQuery('#tie_post_slider-item, #tie_googlemap_url-item').hide();
			jQuery('#tie_video_url-item, #tie_embed_code-item').fadeIn();
		}
		if (selected_item == 'slider') {
			jQuery('#tie_video_url-item, #tie_embed_code-item, #tie_googlemap_url-item').hide();
			jQuery('#tie_post_slider-item').fadeIn();
		}
		if (selected_item == 'map') {
			jQuery('#tie_video_url-item, #tie_embed_code-item, #tie_post_slider-item').hide();
			jQuery('#tie_googlemap_url-item').fadeIn();
		}
		if (selected_item == 'thumb' || selected_item == 'none' || selected_item == '') {
			jQuery('#tie_video_url-item, #tie_embed_code-item, #tie_post_slider-item, #tie_googlemap_url-item').hide();
		}
	 });

	 
// Display on Home ##############################################
	var selected_radio = jQuery("input[name='tie_options[on_home]']:checked").val();
	if (selected_radio == 'latest') {	jQuery('#Home_Builder').hide();	}
	jQuery("input[name='tie_options[on_home]']").change(function(){
		var selected_radio = jQuery("input[name='tie_options[on_home]']:checked").val();
		if (selected_radio == 'latest') {
			jQuery('#Home_Builder').fadeOut();
		}else{
			jQuery('#Home_Builder').fadeIn();
		}
	 });
	 
	 
// Slider Position ##############################################
	var selected_pos = jQuery("input[name='tie_options[slider_type]']:checked").val();
	
	if (selected_pos == 'elastic') {jQuery('#elastic').show();}
	if (selected_pos == 'flexi') {jQuery('#flexi').show();}

	jQuery("input[name='tie_options[slider_type]']").change(function(){
		var selected_pos = jQuery("input[name='tie_options[slider_type]']:checked").val();
		if (selected_pos == 'elastic') {
			jQuery('#flexi').hide();
			jQuery('#elastic').fadeIn();
		}
		if (selected_pos == 'flexi') {
			jQuery('#elastic').hide();
			jQuery('#flexi').fadeIn();
		}

	 });

	 
// Slider Query Type ##############################################
	var selected_type = jQuery("input[name='tie_options[slider_query]']:checked").val();
	
	if (selected_type == 'category') {jQuery('#slider_cat-item').show();}
	if (selected_type == 'tag') {jQuery('#slider_tag-item').show();}
	if (selected_type == 'post') {jQuery('#slider_posts-item').show();}
	if (selected_type == 'page') {jQuery('#slider_pages-item').show();}
	if (selected_type == 'custom') {jQuery('#slider_custom-item').show();}
	
	jQuery("input[name='tie_options[slider_query]']").change(function(){
		var selected_type = jQuery("input[name='tie_options[slider_query]']:checked").val();
		if (selected_type == 'category') {
			jQuery('#slider_tag-item ,#slider_posts-item ,#slider_pages-item,#slider_custom-item').hide();
			jQuery('#slider_cat-item').fadeIn();
		}
		if (selected_type == 'tag') {
			jQuery('#slider_cat-item ,#slider_posts-item ,#slider_pages-item,#slider_custom-item').hide();
			jQuery('#slider_tag-item').fadeIn();
		}
		if (selected_type == 'post') {
			jQuery('#slider_cat-item ,#slider_tag-item ,#slider_pages-item,#slider_custom-item').hide();
			jQuery('#slider_posts-item').fadeIn();
		}
		if (selected_type == 'page') {
			jQuery('#slider_cat-item ,#slider_posts-item ,#slider_tag-item,#slider_custom-item').hide();
			jQuery('#slider_pages-item').fadeIn();
		}
		if (selected_type == 'custom') {
			jQuery('#slider_cat-item ,#slider_posts-item ,#slider_tag-item,#slider_pages-item').hide();
			jQuery('#slider_custom-item').fadeIn();
		}
	 });
 
	
// Save Settings Alert	##############################################
	jQuery(".mpanel-save").click( function() {
		jQuery('#save-alert').fadeIn();
	});


// HomeBuilder
	var htm1l = jQuery('#cats_defult').html();
	
	jQuery("#add-cat").click(function() {
		jQuery('#cat_sortable').append('	<li id="listItem_'+ nextCell +'" class="ui-state-default"><div class="widget-head"> News Box <a style="display:none" class="toggle-open">+</a><a style="display:block" class="toggle-close">-</a></div><div style="display:block" class="widget-content"><label><span>Box Category : </span><select name="tie_home_cats['+ nextCell +'][id]" id="tie_home_cats['+ nextCell +'][id]">'+htm1l+'</select></label><label for="tie_home_cats['+ nextCell +'][number]"><span>Number of posts to show :</span><input style="width:50px;" id="tie_home_cats['+ nextCell +'][number]" name="tie_home_cats['+ nextCell +'][number]" value="5" type="text" /></label><label><span style="float:left; width:162px">Box Style : </span><ul class="tie-cats-options tie-options"><li class="selected"><input id="tie_home_cats['+ nextCell +'][style]" name="tie_home_cats['+ nextCell +'][style]" type="radio" value="li" checked="checked"/><a class="checkbox-select" href="#"><img src="'+ templatePath +'/panel/images/li.png" /></a></li><li><input id="tie_home_cats['+ nextCell +'][style]" name="tie_home_cats['+ nextCell +'][style]" type="radio" value="2c" /><a class="checkbox-select" href="#"><img src="'+ templatePath +'/panel/images/2c.png" /></a></li><li><input id="tie_home_cats['+ nextCell +'][style]" name="tie_home_cats['+ nextCell +'][style]" type="radio" value="1c" /><a class="checkbox-select" href="#"><img src="'+ templatePath +'/panel/images/1c.png" /></a></li></ul></label><input id="tie_home_cats['+ nextCell +'][type]" name="tie_home_cats['+ nextCell +'][type]" value="n" type="hidden" /><a class="del-cat"></a></div></li>');
		jQuery('#listItem_'+ nextCell).hide().fadeIn();
		nextCell ++ ;
	});
	jQuery("#add-slider").click(function() {
		jQuery('#cat_sortable').append('<li id="listItem_'+ nextCell +'" class="ui-state-default"><div class="widget-head"> Scrolling Box <a style="display:none" class="toggle-open">+</a><a style="display:block" class="toggle-close">-</a></div><div class="widget-content" style="display:block"><label><span>Box Category : </span><select name="tie_home_cats['+ nextCell +'][id]" id="tie_home_cats['+ nextCell +'][id]">'+htm1l+'</select></label><label for="tie_home_cats['+ nextCell +'][title]"><span>Box Title :</span><input id="tie_home_cats['+ nextCell +'][title]" name="tie_home_cats['+ nextCell +'][title]" value="Scrolling Box" type="text" /></label><label for="tie_home_cats['+ nextCell +'][number]"><span>Number of posts to show :</span><input style="width:50px;" id="tie_home_cats['+ nextCell +'][number]" name="tie_home_cats['+ nextCell +'][number]" value="12" type="text" /></label><input id="tie_home_cats['+ nextCell +'][type]" name="tie_home_cats['+ nextCell +'][type]" value="s" type="hidden" /><a class="del-cat"></a></div></li>');
		jQuery('#listItem_'+ nextCell).hide().fadeIn();
		nextCell ++ ;
	});
	jQuery("#add-news-picture").click(function() {
		jQuery('#cat_sortable').append('<li id="listItem_'+ nextCell +'" class="ui-state-default"><div class="widget-head">  News In Picture <a style="display:none" class="toggle-open">+</a><a style="display:block" class="toggle-close">-</a></div><div class="widget-content" style="display:block"><label><span>Box Category : </span><select name="tie_home_cats['+ nextCell +'][id]" id="tie_home_cats['+ nextCell +'][id]">'+htm1l+'</select></label><label for="tie_home_cats['+ nextCell +'][title]"><span>Box Title :</span><input id="tie_home_cats['+ nextCell +'][title]" name="tie_home_cats['+ nextCell +'][title]" value="News In Picture" type="text" /></label><input id="tie_home_cats['+ nextCell +'][type]" name="tie_home_cats['+ nextCell +'][type]" value="news-pic" type="hidden" /><a class="del-cat"></a></div></li>');
		jQuery('#listItem_'+ nextCell).hide().fadeIn();
		nextCell ++ ;
	});
	jQuery("#add-divider").click(function() {
		jQuery('#cat_sortable').append('<li id="listItem_'+ nextCell +'" class="ui-state-default"><div class="widget-head divider"> Divider <a style="display:none" class="toggle-open">+</a><a style="display:block" class="toggle-close">-</a></div><div class="widget-content" style="display:block"><label for="tie_home_cats[<?php echo jQueryi ?>][height]"><span>Height :</span><input id="tie_home_cats['+ nextCell +'][type]" name="tie_home_cats['+ nextCell +'][type]" value="divider" type="hidden" />  <input id="tie_home_cats['+ nextCell +'][height]" name="tie_home_cats['+ nextCell +'][height]" value="10" type="text" style="width:50px;" /> px</label>  <a class="del-cat"></a></div></li>');
		jQuery('#listItem_'+ nextCell).hide().fadeIn();
		nextCell ++ ;
	});
	jQuery("#add-recent").click(function() {
		jQuery('#cat_sortable').append('<li id="listItem_'+ nextCell +'" class="ui-state-default"><div class="widget-head">  Recent Posts  <a style="display:none" class="toggle-open">+</a><a style="display:block" class="toggle-close">-</a></div><div style="display:block" class="widget-content"><label><span style="float:left;">Exclude This Categories : </span><select multiple="multiple" name="tie_home_cats['+ nextCell +'][exclude][]" id="tie_home_cats['+ nextCell +'][exclude][]">'+htm1l+'</select></label><label for="tie_home_cats['+ nextCell +'][title]"><span>Box Title :</span><input id="tie_home_cats['+ nextCell +'][title]" name="tie_home_cats['+ nextCell +'][title]" value="Recent Posts" type="text" /></label><label for="tie_home_cats['+ nextCell +'][number]"><span>Number of posts to show :</span><input style="width:50px;" id="tie_home_cats['+ nextCell +'][number]" name="tie_home_cats['+ nextCell +'][number]" value="3" type="text" /></label><label for="tie_home_cats[<?php echo $i ?>][display]"><span>Display Mode:</span><select id="tie_home_cats['+ nextCell +'][display]" name="tie_home_cats['+ nextCell +'][display]"><option value="default">Default Style</option><option value="blog">Blog Style</option></select></label><input id="tie_home_cats['+ nextCell +'][type]" name="tie_home_cats['+ nextCell +'][type]" value="recent" type="hidden" /><a class="del-cat"></a></div></li>');
		jQuery('#listItem_'+ nextCell).hide().fadeIn();
		nextCell ++ ;
	});
	
	jQuery("#add-ads").click(function() {
		jQuery('#cat_sortable').append('<li id="listItem_'+ nextCell +'" class="ui-state-default"><div class="widget-head">  ADS <a style="display:none" class="toggle-open">+</a><a style="display:block" class="toggle-close">-</a></div><div class="widget-content" style="display:block"><textarea name="tie_home_cats['+ nextCell +'][text]" id="tie_home_cats['+ nextCell +'][text]"></textarea><input id="tie_home_cats['+ nextCell +'][type]" name="tie_home_cats['+ nextCell +'][type]" value="ads" type="hidden" /><a class="del-cat"></a></div></li>');
		jQuery('#listItem_'+ nextCell).hide().fadeIn();
		nextCell ++ ;
	});
	
	
	jQuery(".toggle-open").live("click" ,function () {
		jQuery(this).parent().parent().find(".widget-content").slideToggle(300);
		jQuery(this).hide();
		jQuery(this).parent().find(".toggle-close").show();
    });

	jQuery(".toggle-close").live("click" ,function () {
		jQuery(this).parent().parent().find(".widget-content").slideToggle("fast");
		jQuery(this).hide();
		jQuery(this).parent().find(".toggle-open").show();
    });
	
	
	jQuery("#expand-all").live("click" ,function () {
		jQuery("#cat_sortable .widget-content").slideDown(300);
		jQuery("#cat_sortable .toggle-close").show();
		jQuery("#cat_sortable .toggle-open").hide();
    });
	jQuery("#collapse-all").live("click" ,function () {
		jQuery("#cat_sortable .widget-content").slideUp(300);
		jQuery("#cat_sortable .toggle-close").hide();
		jQuery("#cat_sortable .toggle-open").show();
    });
	
	
// Del Cats ##############################################
	jQuery(".del-cat").live("click" , function() {
		jQuery(this).parent().parent().addClass('removered').fadeOut(function() {
			jQuery(this).remove();
		});
	});


// Delete Sidebars Icon ##############################################
	jQuery(".del-sidebar").live("click" , function() {
		var option = jQuery(this).parent().find('input').val();
		jQuery(this).parent().parent().addClass('removered').fadeOut(function() {
			jQuery(this).remove();
			jQuery('#custom-sidebars select').find('option[value="'+option+'"]').remove();

		});
	});	
	
	
// Sidebar Builder ##############################################
	jQuery("#sidebarAdd").click(function() {
		var SidebarName = jQuery('#sidebarName').val();
		if( SidebarName.length > 0){
			jQuery('#sidebarsList').append('<li><div class="widget-head">'+SidebarName+' <input id="tie_sidebars" name="tie_options[sidebars][]" type="hidden" value="'+SidebarName+'" /><a class="del-sidebar"></a></div></li>');
			jQuery('#custom-sidebars select').append('<option value="'+SidebarName+'">'+SidebarName+'</option>');
		}
		jQuery('#sidebarName').val('');

	});
	

// Background Type ##############################################
	var bg_selected_radio = jQuery("input[name='tie_options[background_type]']:checked").val();
	if (bg_selected_radio == 'custom') {	jQuery('#pattern-settings').hide();	}
	if (bg_selected_radio == 'pattern') {	jQuery('#bg_image_settings').hide();	}
	jQuery("input[name='tie_options[background_type]']").change(function(){
		var bg_selected_radio = jQuery("input[name='tie_options[background_type]']:checked").val();
		if (bg_selected_radio == 'pattern') {
			jQuery('#pattern-settings').fadeIn();
			jQuery('#bg_image_settings').hide();
		}else{
			jQuery('#bg_image_settings').fadeIn();
			jQuery('#pattern-settings').hide();
		}
	 });
 
	
			
		
	jQuery('a[rel=tooltip]').mouseover(function(e) {
		var tip = jQuery(this).attr('title');    
		jQuery(this).attr('title','');
		jQuery(this).append('<div id="tooltip"><div class="tipHeader"></div><div class="tipBody">' + tip +'</div><div class="tipFooter"></div></div>');     
			 
		jQuery('#tooltip').css('top', e.pageY -10 );
		jQuery('#tooltip').css('left', e.pageX - 20 );
			 
		jQuery('#tooltip').fadeIn('500');
		jQuery('#tooltip').fadeTo('10',0.8);
					 
	}).mousemove(function(e) {
				 
		jQuery('#tooltip').css('top', e.pageY -10 );
		jQuery('#tooltip').css('left', e.pageX - 20 );
					 
	}).mouseout(function() {
				 
		jQuery(this).attr('title',jQuery('.tipBody').html());
		jQuery(this).children('div#tooltip').remove();
		 
	});
			

	jQuery(".tabs-wrap").hide();
	jQuery(".mo-panel-tabs ul li:first").addClass("active").show();
	jQuery(".tabs-wrap:first").show(); 
	jQuery("li.tabs").click(function() {
		jQuery(".mo-panel-tabs ul li").removeClass("active");
		jQuery(this).addClass("active");
		jQuery(".tabs-wrap").hide();
		var activeTab = jQuery(this).find("a").attr("href");
		jQuery(activeTab).fadeIn();
		return false;
	});
	
	
	
	jQuery("#theme-skins input:checked").parent().addClass("selected");
	jQuery("#theme-skins .checkbox-select").click(
		function(event) {
			event.preventDefault();
			jQuery("#theme-skins li").removeClass("selected");
			jQuery(this).parent().addClass("selected");
			jQuery(this).parent().find(":radio").attr("checked","checked");			 
		}
	);	
	
	
	jQuery("#theme-pattern input:checked").parent().addClass("selected");
	jQuery("#theme-pattern .checkbox-select").click(
		function(event) {
			event.preventDefault();
			jQuery("#theme-pattern li").removeClass("selected");
			jQuery(this).parent().addClass("selected");
			jQuery(this).parent().find(":radio").attr("checked","checked");			 
		}
	);	
	
	
	jQuery("#sidebar-position-options input:checked").parent().addClass("selected");
	jQuery("#sidebar-position-options .checkbox-select").click(
		function(event) {
			event.preventDefault();
			jQuery("#sidebar-position-options li").removeClass("selected");
			jQuery(this).parent().addClass("selected");
			jQuery(this).parent().find(":radio").attr("checked","checked");			 
		}
	);	

	
	jQuery("#footer-widgets-options input:checked").parent().addClass("selected");
	jQuery("#footer-widgets-options .checkbox-select").click(
		function(event) {
			event.preventDefault();
			jQuery("#footer-widgets-options li").removeClass("selected");
			jQuery(this).parent().addClass("selected");
			jQuery(this).parent().find(":radio").attr("checked","checked");			 
		}
	);	


	jQuery(".tie-cats-options input:checked").parent().addClass("selected");
	jQuery(".tie-cats-options .checkbox-select").live("click" , function(event) {
		event.preventDefault();
		jQuery(this).parent().parent().find("li").removeClass("selected");
		jQuery(this).parent().addClass("selected");
		jQuery(this).parent().find(":radio").attr("checked","checked");			 
		
	});
	
	
	jQuery("#tabs_cats input:checked").parent().addClass("selected");
	jQuery("#tabs_cats span").click(
		function(event) {
			event.preventDefault();
			if( jQuery(this).parent().find(":checkbox").is(':checked') ){
				jQuery(this).parent().removeClass("selected");
				jQuery(this).parent().find(":checkbox").removeAttr("checked");			 
			}else{
				jQuery(this).parent().addClass("selected");
				jQuery(this).parent().find(":checkbox").attr("checked","checked");
			}				
		}
	);
	 

});
		
function toggleVisibility(id) {
	var e = document.getElementById(id);
    if(e.style.display == 'block')
          e.style.display = 'none';
       else
          e.style.display = 'block';
}	
