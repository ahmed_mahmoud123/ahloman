<?php
/**
 * Template Name: Advanced Search
 */
?>

<?php get_header(); ?>
		<div class="box">
			<div class="boxtop">
				<div class="titlebg">
					<div class="title">
						<div class="icon icon01"></div> البحث المتقدم
					</div>
				</div>
			</div>
			<div class="boxcenter">
				<div class="search">
					<form class="advancedS" role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
					    <input type="text" value="" name="s" id="s" class="searchinput" /><br />
					    <p> من فضلك إضغط على زر Ctrl لإختيار أكثر من تصنيف للبحث </p>
						<?php //wp_dropdown_categories('show_option_all=جميع الأقسام&class=select'); 
							$select_cats = wp_dropdown_categories( array( 'echo' => 0 ) );
							$select_cats = str_replace( "name='cat' id=", "name='cat[]' multiple='multiple' id=", $select_cats );
							echo $select_cats;
						?>
						
					    <input type="submit" id="searchsubmit" value="بحث" class="button" />
					</form>
				</div>
			</div>
		</div>
<?php get_footer(); ?>