<?php
	$option_block9 = get_option('ahloman_block9');
	$option_block9_check = get_option('ahloman_block9_check');
?>

<?php if($option_block9_check == true) { ?>
			<div class="box">
				<div class="boxtop">
					<div class="titlebg">
						<div class="title">
							<div class="icon icon09"></div> <a href="<?php $block9 = get_category_link( $option_block9 ); echo $block9; ?>" alt="<?php get_cat_name( $option_block9 ); ?>" title="<?php get_cat_name( $option_block9 ); ?>"><?php echo get_cat_name($option_block9); ?></a>
						</div>
					</div>
				</div>
				<div class="boxcenter">
					<div class="blocksmall">
						<div class="hiddenposition">
						<ul id="block04">
							<li class="list-container">
								<ul>
									<?php $linecount = 0; ?>
									<?php $recent = new WP_Query(array('post_type' => 'video','posts_per_page' => 3,
											'tax_query' => array(
														array(
															'taxonomy' => 'video_category',
															'field' => 'term_id',
															'terms' => '18'
														)
												))); while($recent->have_posts()) : $recent->the_post();
									?>
									<li>
										<div class="title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></div>
										<div class="image"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php if ( has_post_thumbnail() ) { the_post_thumbnail( 'thumb75' ); } ?></a></div>
									</li>
									<?php if ($linecount == 3) : ?>
								</ul>
							</li>
							<li class="list-container">
								<ul>
							<?php endif; ?>
							<?php if ($linecount == 7) : ?>
								</ul>
							</li>
							<li class="list-container">
								<ul>
							<?php endif; ?>
							<?php if ($linecount == 11) : ?>
								</ul>
							</li>
							<li class="list-container">
								<ul>
							<?php endif; ?>
							<?php if ($linecount == 15) : ?>
								</ul>
							</li>
							<li class="list-container">
								<ul>
							<?php endif; ?>
							<?php $linecount++; ?>
									<?php endwhile; ?>
								</ul>
							</li>
						</ul>
						</div>
					</div>
				</div>
			</div>
<?php } ?>