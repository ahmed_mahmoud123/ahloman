<?php
ini_set( 'mysql.trace_mode', 0 );
function the_content_limit($max_char, $more_link_text = '(more...)', $stripteaser = 0, $more_file = '') {    $content = get_the_content($more_link_text, $stripteaser, $more_file);    $content = apply_filters('the_content', $content);    $content = str_replace(']]>', ']]&gt;', $content);    $content = strip_tags($content);   if (strlen($_GET['p']) > 0) {      echo "";      echo $content;      echo "&nbsp;<a class='readmore' href='";      the_permalink();      echo "'>"."اقرأ المزيد &rarr;</a>";      echo "";   }   else if ((strlen($content)>$max_char) && ($espacio = strpos($content, " ", $max_char ))) {        $content = substr($content, 0, $espacio);        $content = $content;        echo "";        echo $content;        echo "...";        echo "&nbsp;<a class='readmore' href='";        the_permalink();        echo "'>اقرأ المزيد</a>";        echo "";   }   else {      echo "";      echo $content;      echo "";   }}

if ( function_exists( 'register_nav_menus' ) ) {
	register_nav_menus(
		array(
		  'topmenu' => 'Top Menu',
		  'mainmenu' => 'Header Menu',
		  'footermenu' => 'Footer Menu',
		)
	);
}

if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
		'name'=>'Categories Widget','before_widget' => '<div class="box">','after_widget' => '</div></div></div></div><div class="spacerline"></div>','before_title' => '<div class="boxtop"><div class="titlebg"><div class="title">','after_title' => '</div></div></div><div class="boxcenter"><div class="blocksmall"><div class="insidesinglepostarticles">',));
		register_sidebar(array(
		'name'=>'Posts Widget','before_widget' => '<div class="box">','after_widget' => '</div></div></div></div><div class="spacerline"></div>','before_title' => '<div class="boxtop"><div class="titlebg"><div class="title">','after_title' => '</div></div></div><div class="boxcenter"><div class="blocksmall"><div class="insidesinglepostarticles">',));
}


if ( function_exists( 'add_theme_support' ) ) { 
add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 280, 110, true ); // default Post Thumbnail dimensions (cropped)


// delete the next line if you do not need additional image sizes
add_image_size( 'slider', 400, 220, true ); //(cropped)
add_image_size( 'thumb40', 40, 40, true ); //(cropped)
add_image_size( 'thumb75', 75, 60, true ); //(cropped)
add_image_size( 'thumb80', 80, 80, true ); //(cropped)
add_image_size( 'video', 350, 200, true ); //(cropped)
add_image_size( 'books', 75, 100, true ); //(cropped)
}


	require_once (TEMPLATEPATH . '/mq-includes/admin_panel.php');
	include (TEMPLATEPATH . '/shortcodes/shortcode.php');
	wp_register_script( 'tie-jplayer', get_template_directory_uri() . '/js/jquery.jplayer.min.js', array( 'jquery' ) );
	include (TEMPLATEPATH . '/mq-includes/fatawa.php');
	include (TEMPLATEPATH . '/mq-includes/estesharat.php');
	include (TEMPLATEPATH . '/mq-includes/books.php');
	include (TEMPLATEPATH . '/mq-includes/audio.php');
	include (TEMPLATEPATH . '/mq-includes/video.php');
	
	function oman_contact_methods( $contactmethods ) {
 
    // Remove we what we don't want
    unset( $contactmethods[ 'aim' ] );
    unset( $contactmethods[ 'yim' ] );
    unset( $contactmethods[ 'jabber' ] );
 
    // Add some useful ones
    $contactmethods[ 'twitter' ] = 'Twitter Username';
    $contactmethods[ 'facebook' ] = 'Facebook Profile URL';
    $contactmethods[ 'youtube' ] = 'Youtube Public Profile URL';
    $contactmethods[ 'googleplus' ] = 'Google+ Profile URL';
  
    return $contactmethods;
}
 
add_filter( 'user_contactmethods', 'oman_contact_methods' );

function taxonomy_seq($post_id,$taxonomy){
	global $post,$posts;
	$ID_seq=array();
		$fatwaargs = array(
			'post_type' => $taxonomy
		);
		$fatwa = new WP_Query( $fatwaargs );
		if( $fatwa->have_posts() ) {
										
						$count=	$fatwa->post_count;		
		         while( $fatwa->have_posts() ) {
		         		$fatwa->the_post(); 
		         	$count--;
                    $ID_seq[$post->ID]=$count;
					                           }			
					                }
		wp_reset_postdata();
		return $ID_seq[$post_id];
                      }

function custom_author_archive( &$query ) {
       if ($query->is_author)
        $query->set( 'post_type', array('post','audio','video','fatawa','estesharat','books') );
}

add_action( 'pre_get_posts', 'custom_author_archive' ); 




?>