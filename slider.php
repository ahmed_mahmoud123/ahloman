<?php
	// Head Master Articles
	$option_headmasterarticles = get_option('ahloman_headmasterarticles');
	$option_headmasterarticles_check = get_option('ahloman_headmasterarticles_check');
?>

	<div id="sliderbox">
		<div class="leftcontent">
			<div class="social">
				<a href="http://ahloman.net/?feed=rss2" target="_blank" class="rss"></a>
				<a href="http://www.facebook.com/ahloman" target="_blank" class="facebook"></a>
				<a href="http://www.twitter.com/ahloman" target="_blank" class="twitter"></a>
				<a href="http://plus.google.com/100217815769757615246" target="_blank" class="gplus"></a>
				<a href="http://www.youtube.com/user/ahlomanNet" target="_blank" class="youtube"></a>
			</div>

			<?php if($option_headmasterarticles_check == true) { ?>
			<div class="headmaster">
				<h1><a href="<?php $headmasterarticles = get_category_link( $option_headmasterarticles ); echo $headmasterarticles; ?>" alt="<?php get_cat_name( $option_headmasterarticles ); ?>" title="<?php get_cat_name( $option_headmasterarticles ); ?>"><?php echo get_cat_name($option_headmasterarticles); ?></a></h1>
				<div class="content">
					<ul id="catheadmaster">
						<?php $recent = new WP_Query("cat=".$option_headmasterarticles."&showposts=5&orderby=last"); while($recent->have_posts()) : $recent->the_post();?>
						<li>
							<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
							<h3>أضيف بتاريخ: <?php the_time('j M Y'); ?></h3>
							<p><?php the_content_limit(200,''); ?></p>
						</li>
						<?php endwhile; ?>
					</ul>
				</div>
			</div>
			<?php } ?>
		</div>
		<?php include(TEMPLATEPATH."/homeblocks/sliderbox.php"); ?>
	</div>