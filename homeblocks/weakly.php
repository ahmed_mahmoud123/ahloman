<?php
	$option_weaklyblock = get_option('ahloman_weaklyblock');
	$option_weaklyblock_check = get_option('ahloman_weaklyblock_check');
	
	$option_footertext = get_option('ahloman_footertext');
    $option_ad_link = get_option('ahloman_ad_link');
?>	


		<div class="bigbanner"><a target="_blank" href="<?php echo $option_ad_link ?>"><img width="650" src="<?php echo $option_footertext ?>" /></a></div>
			<?php if($option_weaklyblock_check == true) { ?>
			<div class="box">
				<div class="boxtop">
					<div class="titlebg">
						<div class="title">
							<div class="icon icon01"></div> <a href="<?php $weaklyblock = get_category_link( $option_weaklyblock ); echo $weaklyblock; ?>" alt="<?php get_cat_name( $option_weaklyblock ); ?>" title="<?php get_cat_name( $option_weaklyblock ); ?>"><?php echo get_cat_name($option_weaklyblock); ?></a>
						</div>
					</div>
				</div>
				<div class="boxcenter">
					<div class="weeklyword">
						<?php $recent = new WP_Query("cat=".$option_weaklyblock."&showposts=1&orderby=last"); while($recent->have_posts()) : $recent->the_post();?>
						<h1><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h1>
						<p><?php the_content_limit(900,''); ?></p>
						<?php endwhile; ?>
						<div class="spacerline"></div>
					</div>
				</div>
			</div>
			<?php } ?>