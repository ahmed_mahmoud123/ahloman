<?php
	$option_block4 = get_option('ahloman_block4');
	$option_block4_check = get_option('ahloman_block4_check');
	$option_block5 = get_option('ahloman_block5');
	$option_block5_check = get_option('ahloman_block5_check');
	$option_block6 = get_option('ahloman_block6');
	$option_block6_check = get_option('ahloman_block6_check');
	$option_block7 = get_option('ahloman_block7');
	$option_block7_check = get_option('ahloman_block7_check');
	$option_multicatblock_check = get_option('ahloman_multicatblock_check');
?>

<?php if($option_multicatblock_check == true) { ?>
			<div class="box" id="multicatblock">
				<div class="boxtop">
					<div class="titlebg">
						<div class="title">
							<ul>
								<?php if($option_block4_check == true) { ?><li><a href="#multicatblock01"><?php echo get_cat_name($option_block4); ?></a></li><?php } ?>
								<?php if($option_block5_check == true) { ?><li><a href="#multicatblock02"><?php echo get_cat_name($option_block5); ?></a></li><?php } ?>
								<?php if($option_block6_check == true) { ?><li><a href="#multicatblock03"><?php echo get_cat_name($option_block6); ?></a></li><?php } ?>
								<?php if($option_block7_check == true) { ?><li><a href="#multicatblock04"><?php echo get_cat_name($option_block7); ?></a></li><?php } ?>
							</ul>
						</div>
					</div>
				</div>
				<div class="boxcenter">
					<?php if($option_block4_check == true) { ?>
					<div class="multicat" id="multicatblock01">
						<div id="multicatsubblock01">
						<ul>
							<?php
							
							 $recent = new WP_Query("post_type=audio&audio_category=قرأن كريم&showposts=6&orderby=last"); while($recent->have_posts()) : $recent->the_post();?>
							<li><a href="#multicatsubblock01-<?php the_ID(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
							<?php endwhile; ?>
						</ul>
						<?php
						
						global $post;
							$recent = new WP_Query("post_type=audio&audio_category=قرأن كريم&showposts=6&orderby=last"); while($recent->have_posts()) : $recent->the_post();?>
							
				
					<div class="contenttext" id="multicatsubblock01-<?php the_ID(); ?>">
						
							<?php $audio_text = get_post_meta($post->ID, 'audio_text', true);  ?>
							<audio class="auto-player-single" controls preload="none">
							  <source src="<?php echo $audio_text ?>" type="audio/mpeg">
							Your browser does not support the audio element.
						</audio>
					</div>
						<?php endwhile; ?>
						</div>
					</div>
					<?php } ?>
					
					<?php if($option_block5_check == true) { ?>
					<div class="multicat" id="multicatblock02">
						<div id="multicatsubblock02">
						<ul>
							<?php $recent = new WP_Query("post_type=audio&audio_category=محاضرات عامة&showposts=6&orderby=last"); while($recent->have_posts()) : $recent->the_post();?>
							<li><a href="#multicatsubblock02-<?php the_ID(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
							<?php endwhile; ?>
						</ul>
								<?php
								global $post;
							$recent = new WP_Query("post_type=audio&audio_category=محاضرات عامة&showposts=6&orderby=last"); while($recent->have_posts()) : $recent->the_post();?>
							
				
					<div class="contenttext" id="multicatsubblock02-<?php the_ID(); ?>">
						
							<?php $audio_text = get_post_meta($post->ID, 'audio_text', true);  ?>
							<audio class="auto-player-single" controls preload="none">
							  <source src="<?php echo $audio_text ?>" type="audio/mpeg">
							Your browser does not support the audio element.
						</audio>
					</div>
						<?php endwhile; ?>
						
							</div>
					</div>
					<?php } ?>
					
					<?php if($option_block6_check == true) { ?>
					<div class="multicat" id="multicatblock03">
						<div id="multicatsubblock03">
						<ul>
							<?php $recent = new WP_Query("post_type=audio&audio_category=دروس علمية&showposts=6&orderby=last"); while($recent->have_posts()) : $recent->the_post();?>
							<li><a href="#multicatsubblock03-<?php the_ID(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
							<?php endwhile; ?>
						</ul>
							<?php
								global $post;
							$recent = new WP_Query("post_type=audio&audio_category=دروس علمية&showposts=6&orderby=last"); while($recent->have_posts()) : $recent->the_post();?>
							
				
					<div class="contenttext" id="multicatsubblock03-<?php the_ID(); ?>">
						
						
							<?php $audio_text = get_post_meta($post->ID, 'audio_text', true);  ?>
							
							<audio class="auto-player-single" controls preload="none">
							
							  <source src="<?php echo $audio_text ?>" type="audio/mpeg">
							Your browser does not support the audio element.
						</audio>
					</div>
						<?php endwhile; ?>
						
							</div>
					</div>
					<?php } ?>
					
					<div class="multicat" id="multicatblock04">
						<div id="multicatsubblock04">
						<ul>
							<?php 
								$recent = new WP_Query("post_type=estesharat&showposts=6&orderby=last"); while($recent->have_posts()) : $recent->the_post();
								
								
							?>
							<li><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></li>
							
							<?php endwhile; ?>
						</ul>
						</div>
					</div>
					
					<div class="spacerline"></div>
				</div>
			</div>
<?php } ?>