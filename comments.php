<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

// Do not delete these lines
	if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die ('لا تقم بطلب هذه الصفحة مباشرة، شكرا جزيلا لك.');

	if ( post_password_required() ) { ?>
		<p class="nocomments">هذا المحتوى محفوظ بكلمة مرور.</p>
	<?php
		return;
	}
?>

<!-- You can start editing here. -->
<br />
<?php if ( have_comments() ) : ?>
	<h1 id="comments"><?php comments_number('بدون تعليقات', 'تعليق واحد', '% من التعليقات' );?> لـ &#8220;<?php the_title(); ?>&#8221;</h3>

	<div class="navigation">
		<div class="alignleft"><?php previous_comments_link() ?></div>
		<div class="alignright"><?php next_comments_link() ?></div>
	</div>

<ol class="commentlist">
<?php foreach ($comments as $comment) : ?>
<li <?php echo $oddcomment; ?>id="comment-<?php comment_ID() ?>">
<cite>
	المعلق
	<span class="author"><?php comment_author_link() ?></span>
	 في 
	<span class="time"><?php comment_time() ?></span>
	<a href="#comment-<?php comment_ID() ?>" title=""><?php comment_date('n-j-Y') ?></a> <?php edit_comment_link('تعديل','&nbsp;&nbsp;',''); ?>
</cite>
<div class="commenttext"><?php comment_text() ?></div>
<?php if ($comment->comment_approved == '0') : ?>
<em>تعليقك بإنتظار التفعيل الإداري.</em>
<?php endif; ?>
</li>
<?php
/* Changes every other comment to a different class */
$oddcomment = ( empty( $oddcomment ) ) ? 'class="alt" ' : '';
?>
<?php endforeach; /* end for each comment */ ?>
</ol>

	<div class="navigation">
		<div class="alignleft"><?php previous_comments_link() ?></div>
		<div class="alignright"><?php next_comments_link() ?></div>
	</div>
 <?php else : // this is displayed if there are no comments so far ?>

	<?php if ('open' == $post->comment_status) : ?>
		<!-- If comments are open, but there are no comments. -->

	 <?php else : // comments are closed ?>
		<!-- If comments are closed. -->
		<p class="nocomments"></p>

	<?php endif; ?>
<?php endif; ?>


<?php if ('open' == $post->comment_status) : ?>

<div id="respond">

<h1><?php comment_form_title( 'اترك تعليقك', 'إكتب تعليقاً لـ %s' ); ?></h1>

<div class="cancel-comment-reply">
	<small><?php cancel_comment_reply_link(); ?></small>
</div>

<?php if ( get_option('comment_registration') && !$user_ID ) : ?>
<p>يجب أن <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php echo urlencode(get_permalink()); ?>">تسجل دخولك</a> لكتابة تعليق.</p>
<?php else : ?>

<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">

<?php if ( $user_ID ) : ?>

<p align="left" dir="rtl">مسجل دخولك بإسم <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="تسجيل الخروج من هذا الحساب">[خروج]</a></p>

<?php else : ?>

<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse" bordercolor="#111111" width="100%" dir="rtl">
  <tr>
    <td width="35%"><label for="author"><small>الإسم: <?php if ($req) echo "(مطلوب)"; ?></small></label></td>
    <td width="65%"><input type="text" name="author" id="author" value="<?php echo $comment_author; ?>" size="22" tabindex="1" <?php if ($req) echo "aria-required='true'"; ?> /></td>
  </tr>
  <tr>
    <td width="35%"><label for="email"><small>البريد الإلكتروني (لن يتم نشره) <?php if ($req) echo "(مطلوب)"; ?></small></label></td>
    <td width="65%"><input type="text" name="email" id="email" value="<?php echo $comment_author_email; ?>" size="22" tabindex="2" <?php if ($req) echo "aria-required='true'"; ?> /></td>
  </tr>
  <tr>
    <td width="35%"><label for="url"><small>رابط موقعك (غير مطلوب)</small></label></td>
    <td width="65%"><input type="text" name="url" id="url" value="<?php echo $comment_author_url; ?>" size="22" tabindex="3" /></td>
  </tr>
</table>
<br />

<?php endif; ?>

<!--<p><small><strong>XHTML:</strong> يمكنك إستخدام هذه الوسوم: <code><?php echo allowed_tags(); ?></code></small></p>-->

<textarea name="comment" id="comment" cols="100%" rows="10" tabindex="4"></textarea><br />
<input name="submit" type="submit" id="submit" tabindex="5" value="إرسال التعليق" /><br />
<?php comment_id_fields(); ?>

<?php do_action('comment_form', $post->ID); ?>

</form>

<?php endif; // If registration required and not logged in ?>
</div>

<?php endif; // if you delete this the sky will fall on your head ?>
