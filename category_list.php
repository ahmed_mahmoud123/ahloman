<?php
/* 
 * Template Name: Category	
 */
 get_header(); ?>

<?php include(TEMPLATEPATH."/sidebar.php"); ?>
		<div class="topcontentright">
			<div class="box">
				<div class="boxtop">
					<div class="titlebg">
						<div class="title">
							<div class="icon icon11"></div> <?php wp_title(''); ?>
						</div>
					</div>
				</div>
				<div class="boxcenter">
					<a style="font-size: 18px;margin-right:10px" href="#showMain" id="collipse">عرض الجميع</a>
					<hr/>
						<ul style="margin-right: 20px;margin-top: 30px" class='collapsibleList' id="test">
						<?php 
					
					         $args = array(
							'show_option_all'    => '',
							'orderby'            => 'name',
							'order'              => 'ASC',
							'style'              => 'list',
							'show_count'         => 1,
							'hide_empty'         => 0,
							'use_desc_for_title' => 1,
							'child_of'           => 22,
							'feed'               => '',
							'feed_type'          => '',
							'feed_image'         => '',
							'exclude'            => '',
							'exclude_tree'       => '',
							'include'            => '',
							'hierarchical'       => 1,
							'title_li'           => __(''),
							'show_option_none'   => __(''),
							'number'             => null,
							'echo'               => 1,
							'depth'              => 0,
							'current_category'   => 0,
							'pad_counts'         => 1,
							'taxonomy'           => 'category',
							'walker'             => null   ,
							 
							);   
		            $cats = get_categories( $args );
		            	
		            foreach( $cats as $cat) {
		             if($cat->parent == 22){
		                    $parent_cat = null;
		                    $head = $cat->name;
		                    $head_id = $cat->cat_ID;
						  $args2 = array(
							'show_option_all'    => '',
							'orderby'            => 'name',
							'order'              => 'ASC',
							'style'              => 'list',
							'show_count'         => 1,
							'hide_empty'         => 0,
							'use_desc_for_title' => 1,
							'child_of'           => $head_id,
							'feed'               => '',
							'feed_type'          => '',
							'feed_image'         => '',
							'exclude'            => '',
							'exclude_tree'       => '',
							'include'            => '',
							'hierarchical'       => 1,
							'title_li'           => __(''),
							'show_option_none'   => __(''),
							'number'             => null,
							'echo'               => 1,
							'depth'              => 0,
							'current_category'   => 0,
							'pad_counts'         => 1,
							'taxonomy'           => 'category',
							'walker'             => null   ,);
							
							 echo "<li><a class='parent-category' href='http://ahloman.net/?cat=". $cat->cat_ID . "'>" . $head .'('.$cat->count.")</a><ul style=\"margin-right: 20px\">";                                      
		                  
		                    wp_list_categories($args2);
		                    
						    echo "</ul></li>";
					  
					  }
		                }
		               
		            
						?>
					</ul>
					
					
					
					
						
				</div>
				
			</div>
		</div>
<?php get_footer(); ?>