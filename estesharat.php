<?php
/* 
 * Template Name: Estesharat
 */
 get_header(); ?>
<?php
		$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
		$estesharatargs = array(
			'post_type' => 'estesharat',
					
		);
		$estesharat = new WP_Query( $estesharatargs );
		
?>
<?php include(TEMPLATEPATH."/sidebar.php"); ?>
		<div class="topcontentright">
			<?php if(have_posts()): while(have_posts()): the_post() ?>
						<?php the_content() ?>
			<?php endwhile; endif; ?>
			<div class="box">
				<div class="boxtop">
					<div class="titlebg">
						<div class="title">
							<div class="icon icon11"></div> <?php wp_title(''); ?>
						</div>
					</div>
				</div>
				<div class="boxcenter">
					<div class="archivesinglelist">
						
						<ul>
							<li>
								<ul>
									<?php if( $estesharat->have_posts() ) {
										if(!isset($GLOBALS['$count'])){
										$GLOBALS['$count']=$estesharat->post_count-1;		
											}
											while( $estesharat->have_posts() ) {
												$estesharat->the_post();
										$esteshara_question = get_post_meta($post->ID, 'esteshara_question', true); 
										$esteshara_answer = get_post_meta($post->ID, 'esteshara_answer', true);				
									 ?>
									<li>
										<div class="title">
											<a href="<?php the_permalink(); ?>" ><h1><?php the_title() ?></h1></a>
											<span><p style="text-indent: 2px"><strong>المستشار: </strong><?php the_author(); ?></p> </span><br>
											 <p>رقم الاستشارة: <?php echo $GLOBALS['$count']; $GLOBALS['$count']=$GLOBALS['$count']-1; ?></p>
											<a href="<?php the_permalink(); ?>" ><?php echo $esteshara_question ?></a>
										</div>
										<div class="image"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php if ( has_post_thumbnail() ) { the_post_thumbnail( 'thumb75' ); } ?></a></div>
										<div class="date"><span><strong>تاريخ الإضافة:</strong> <?php the_time('d-m-Y'); ?></span><?php if(function_exists('the_views')) { ?><span><strong>زيارة:</strong> <?php the_views(); ?></span><?php } ?><span><strong>التعليقات:</strong> <?php comments_number( 'لا يوجد تعليقات', 'تعليق واحد', '%' ); ?></span><span><?php the_terms( $post->ID, 'estesharat_category', '<strong>التصنيفات: </strong>', ' / ' ); ?></span></div>
									</li>
									<?php
											}
										}
										else { ?>
											<li>
												<div class="title">
													<h1>عفوا</h1>
													<p>عفوا.. لايوجد إستشارات فى الوقت الحالى</p>
												</div>
												<div class="image"></div>
											</li>
										<?php
										}
									?>
								</ul>
							</li>
						</ul>
						<div class="navigation">
							<div class="alignright">
								<?php next_posts_link(' السابق ',$estesharat->max_num_pages) ?>
							</div>
							<div class="alignleft">
								<?php previous_posts_link(' التالي ') ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="box most-post">
				<div class="boxtop">
					<div class="titlebg">
						<div class="title">
							<div class="icon icon11"></div>الأكثر مشاهده
						</div>
					</div>
				</div>
			<div class="boxcenter">
			<div class="archivesinglelist">
						<ul>
							<li>
								<ul>
									<?php  
										$args = array(
													'post_type' => 'estesharat',
													'posts_per_page' => 3,
													'meta_key' => 'views',
													'orderby' => 'meta_value_num',
													'order' => 'DESC',
													); ?>
										<?php query_posts($args); ?>
										<?php if(have_posts() ) 
												{
													
												while(have_posts() ) 
												{
													the_post(); 
										$esteshara_question = get_post_meta($post->ID, 'esteshara_question', true); 
										$esteshara_answer = get_post_meta($post->ID, 'esteshara_answer', true);				
									 ?>
									<li>
										<div class="title">
											<a href="<?php the_permalink(); ?>" ><h1><?php the_title() ?></h1></a>
											<span><p style="text-indent: 2px"><strong>المستشار: </strong><?php the_author(); ?></p> </span><br>
											<p>رقم الاستشارة: <?php echo  taxonomy_seq($post->ID,'estesharat') ?></p>
											<a href="<?php the_permalink(); ?>" ><?php echo $esteshara_question ?></a>
										</div>
										<div class="image"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php if ( has_post_thumbnail() ) { the_post_thumbnail( 'thumb75' ); } ?></a></div>
										<div class="date"><span><strong>تاريخ الإضافة:</strong> <?php the_time('d-m-Y'); ?></span><?php if(function_exists('the_views')) { ?><span><strong>زيارة:</strong> <?php the_views(); ?></span><?php } ?><span><strong>التعليقات:</strong> <?php comments_number( 'لا يوجد تعليقات', 'تعليق واحد', '%' ); ?></span><span><?php the_terms( $post->ID, 'estesharat_category', '<strong>التصنيفات: </strong>', ' / ' ); ?></span></div>
									</li>
									<?php
											}
										}
										else { ?>
											<li>
												<div class="title">
													
													<h1>عفوا</h1>
													<p>عفوا.. لايوجد صوتيات فى الوقت الحالى</p>
												</div>
												<div class="image"></div>
											</li>
										<?php
										}
									?>
								</ul>
							</li>
						</ul>
						
					</div>
				</div>
			</div>
			<div class="box most-post">
				<div class="boxtop">
					<div class="titlebg">
						<div class="title">
							<div class="icon icon11"></div>الأكثر تعليق
						</div>
					</div>
				</div>
			<div class="boxcenter">
			<div class="archivesinglelist">
						<ul>
							<li>
								<ul>
									<?php  
										$args = array(
													'post_type' => 'estesharat',
													'posts_per_page' => 3,
													'orderby' => 'comment_count',
													'order' => 'DESC',
													); ?>
										<?php query_posts($args); ?>
										<?php if(have_posts() ) 
												{
													
												while(have_posts() ) 
												{
													the_post(); 
										$esteshara_question = get_post_meta($post->ID, 'esteshara_question', true); 
										$esteshara_answer = get_post_meta($post->ID, 'esteshara_answer', true);				
									 ?>
									<li>
										<div class="title">
											<a href="<?php the_permalink(); ?>" ><h1><?php the_title() ?></h1></a>
											<span><p style="text-indent: 2px"><strong>المستشار: </strong><?php the_author(); ?></p> </span><br>
											<p>رقم الاستشارة: <?php echo  taxonomy_seq($post->ID,'estesharat') ?></p>
											<a href="<?php the_permalink(); ?>" ><?php echo $esteshara_question ?></a>
										</div>
										<div class="image"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php if ( has_post_thumbnail() ) { the_post_thumbnail( 'thumb75' ); } ?></a></div>
										<div class="date"><span><strong>تاريخ الإضافة:</strong> <?php the_time('d-m-Y'); ?></span><?php if(function_exists('the_views')) { ?><span><strong>زيارة:</strong> <?php the_views(); ?></span><?php } ?><span><strong>التعليقات:</strong> <?php comments_number( 'لا يوجد تعليقات', 'تعليق واحد', '%' ); ?></span><span><?php the_terms( $post->ID, 'estesharat_category', '<strong>التصنيفات: </strong>', ' / ' ); ?></span></div>
									</li>
									<?php
											}
										}
										else { ?>
											<li>
												<div class="title">
													
													<h1>عفوا</h1>
													<p>عفوا.. لايوجد صوتيات فى الوقت الحالى</p>
												</div>
												<div class="image"></div>
											</li>
										<?php
										}
									?>
								</ul>
							</li>
						</ul>
						
					</div>
				</div>
			</div>
		</div>
<?php get_footer(); ?>